package com.lazylad.coupons.lazycoupons;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.lazylad.coupons.lazycoupons.Snippets.CitySnippet;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Amud on 31/01/16.
 */


public class CityAdapter extends RecyclerView.Adapter<CityAdapter.ViewHolder> {

    private ArrayList<CitySnippet> m_city_list;
    private int itemLayout;
    private Context m_activity;
    int viewFlag = 0;
    private SharedPreferences prefs_location;

    public CityAdapter(ArrayList<CitySnippet> items, int itemLayout, Context context, int viewFlag) {
        m_activity = (Activity) context;
        this.m_city_list = items;
        this.itemLayout = itemLayout;
        this.viewFlag = viewFlag;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new ViewHolder(v, viewFlag);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if (viewFlag == 0) {
            if (m_city_list != null) {
                for (int i = 0; i < m_city_list.size(); i++) {
                    Log.e("name", m_city_list.get(i).name);
                }
                final String city_name = ((CitySnippet) getItem(position)).name;

                CitySnippet item = m_city_list.get(position);

                holder.cityNameEditText.setText(city_name);

                holder.cityRelativeLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int m_cityCode = m_city_list.get(holder.getAdapterPosition()).id;
                        String m_cityNameSelected = m_city_list.get(holder.getAdapterPosition()).name;
                        prefs_location = m_activity.getSharedPreferences(
                                "com.lazylad.coupons.lazycoupons", Context.MODE_PRIVATE);

                        SharedPreferences.Editor editor = prefs_location.edit();
                        editor.putInt(EditorConstants.CITY_CODE_CONSTANT, m_cityCode);
                        editor.commit();
                        ((CityActivity)m_activity).startCouponsActivity();

                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return m_city_list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView cityImageView;
        TextView cityNameEditText;
        FrameLayout cityRelativeLayout;

        public ViewHolder(View itemView, int flag) {
            super(itemView);
            if (flag == 0) {
                cityImageView = (ImageView) itemView.findViewById(R.id.city_image);
                cityNameEditText = (TextView) itemView.findViewById(R.id.city_name);
                cityRelativeLayout = (FrameLayout) itemView.findViewById(R.id.relative_layout_city_list);
            }
            if (flag == 1) {

                cityRelativeLayout = (FrameLayout) itemView.findViewById(R.id.relative_layout_city_list);
                cityNameEditText = (TextView) itemView.findViewById(R.id.city_name);
            }
        }
    }

    public Object getItem(int position) {
        if (m_city_list != null) {
            return m_city_list.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


}

