package com.lazylad.coupons.lazycoupons;

/**
 * Created by Saurabh on 20/04/15.
 */
public class EditorConstants {

    public static String VARIFIED_FLAG_PREF_CONSTANT = "VERIFIED_FLAG_PREF";
    public static String VARIFIED_EMAIL_FLAG_PREF_CONSTANT = "VERIFIED_EMAIL_FLAG_PREF";
    public static String SHALL_NUMBER_VERIFY_CONSTANT = "SHALL_NUMBER_VERIFY";

    public static String REFERRAL_JUST_ADDED_CONSTANT = "REFERRAL_JUST_ADDED";
    public static String REFERRAL_CODE_CONSTANT = "REFERRAL_CODE";
    public static String NUMBER_ADDED_CONSTANT = "NUMBER_ADDED";
    public static String NUMBER_JUST_ADDED_CONSTANT = "NUMBER_JUST_ADDED";

    public static String NUMBER_VERIFIED_CONSTANT = "NUMBER_VERIFIED";

    public static String USER_CODE_CONSTANT = "USER_CODE";
    public static String DEFAULT_USER_CODE_CONSTANT = "USER_CODE";

    public static String CITY_CODE_CONSTANT = "CITY_CODE";
    public static int DEFAULT_CITY_CODE_CONSTANT = 0;



}
