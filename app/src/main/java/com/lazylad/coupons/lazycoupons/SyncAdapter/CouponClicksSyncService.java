package com.lazylad.coupons.lazycoupons.SyncAdapter;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by Amud on 29/01/16.
 */
public class CouponClicksSyncService extends Service {
    // Storage for an instance of the sync adapter
    private static CouponClicksSycnAdapter couponClicksSycnAdapter = null;

    // Object to use as a thread-safe lock
    private static final Object sSyncAdapterLock = new Object();

    @Override
    public IBinder onBind(Intent intent) {

        return CouponClicksSyncService.couponClicksSycnAdapter.getSyncAdapterBinder();
    }

    public void onCreate() {

        synchronized (CouponClicksSyncService.sSyncAdapterLock) {
            Log.d("OrdersUpSyncService", "onCreate - OrdersUpSyncService");
            synchronized (sSyncAdapterLock) {
                if (CouponClicksSyncService.couponClicksSycnAdapter == null) {
                    CouponClicksSyncService.couponClicksSycnAdapter = new CouponClicksSycnAdapter(getApplicationContext(), true);
                }
            }
        }
    }
}



