package com.lazylad.coupons.lazycoupons;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;

import com.lazylad.coupons.lazycoupons.SyncAdapter.CouponClicksSycnAdapter;
import com.lazylad.coupons.lazycoupons.SyncAdapter.CouponViewsSycnAdapter;

/**
 * Created by sakshigupta on 28/01/16.
 */
public class SplashScreen extends Activity {


       @Override
    protected void onCreate(Bundle savedInstanceState) {
           super.onCreate(savedInstanceState);

           requestWindowFeature(Window.FEATURE_NO_TITLE);
           setContentView(R.layout.splash_screen);

          // CouponClicksSycnAdapter.syncImmediately(this);


           String user_code;
           SharedPreferences m_sharedPref = getSharedPreferences(
                   "com.lazylad.coupons.lazycoupons", Context.MODE_PRIVATE);
           user_code = m_sharedPref.getString(EditorConstants.USER_CODE_CONSTANT, EditorConstants.DEFAULT_USER_CODE_CONSTANT);
           int city_code = m_sharedPref.getInt(EditorConstants.CITY_CODE_CONSTANT, EditorConstants.DEFAULT_CITY_CODE_CONSTANT);
           Log.d("user_code",user_code);
           if (user_code==EditorConstants.DEFAULT_USER_CODE_CONSTANT) {
               startActivity(new Intent(getApplicationContext(), (LoginActivity.class)));
           } else if (city_code == EditorConstants.DEFAULT_CITY_CODE_CONSTANT) {
               startActivity(new Intent(getApplicationContext(), (CityActivity.class)));
           } else {
               startActivity(new Intent(getApplicationContext(), (CouponClass.class)));
               CouponViewsSycnAdapter.syncImmediately(this);
           }

           if (m_sharedPref.getBoolean("firstLaunch_Account", true)) {
               m_sharedPref.edit().putBoolean("firstLaunch_Account", false).commit();
           }
           SplashScreen.this.finish();
       }
}
