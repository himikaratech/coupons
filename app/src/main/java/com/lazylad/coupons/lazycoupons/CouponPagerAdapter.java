package com.lazylad.coupons.lazycoupons;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by Amud on 25/01/16.
 */


public class CouponPagerAdapter extends FragmentPagerAdapter {

    ArrayList<Integer> m_couponStrips;
    Context m_context;

    public CouponPagerAdapter(FragmentManager fragmentManager,ArrayList<Integer> couponStrips,Context context){
        super(fragmentManager);
        m_couponStrips = couponStrips;
        m_context=context;
    }

    @Override
    public Fragment getItem(int position) {
        SharedPreferences m_sharedPref = m_context.getSharedPreferences(
                "com.lazylad.coupons.lazycoupons", Context.MODE_PRIVATE);
        String userCode = m_sharedPref.getString(EditorConstants.USER_CODE_CONSTANT, EditorConstants.DEFAULT_USER_CODE_CONSTANT);

        Bundle bundle=new Bundle();
        bundle.putString("userCode", userCode);

        if(position==0)
        {
            CouponAllFragment couponAllFragment = new CouponAllFragment ();
            couponAllFragment.setArguments(bundle);
            return couponAllFragment;
        }

        else
        {
            CouponMyFragment couponMyFragment = new CouponMyFragment ();
            couponMyFragment.setArguments(bundle);
            return couponMyFragment;
        }


    }

    @Override
    public int getCount() {
        if(null != ((CouponClass)m_context).m_couponTabNames)
            return ((CouponClass)m_context).m_couponTabNames.size();
        return 0;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return ((CouponClass)m_context).m_couponTabNames.get(position);
    }

}