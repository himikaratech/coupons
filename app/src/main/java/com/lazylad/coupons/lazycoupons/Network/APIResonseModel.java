package com.lazylad.coupons.lazycoupons.Network;

import com.lazylad.coupons.lazycoupons.AccountTransactionsDetailSnippet;
import com.lazylad.coupons.lazycoupons.CouponAllDetailSnippet;
import com.lazylad.coupons.lazycoupons.CouponAllSnippet;
import com.lazylad.coupons.lazycoupons.CouponMyDetailSnippet;
import com.lazylad.coupons.lazycoupons.CouponMySnippet;
import com.lazylad.coupons.lazycoupons.Snippets.CitySnippet;

import java.util.ArrayList;

/**
 * Created by Amud on 25/01/16.
 */
public class APIResonseModel {
    public class LazyPointsResponseModel{
        public int error;
        public int lazy_points;
    }

    public class GetAllCouponResponseModel{
        public boolean success;
        public ArrayList<CouponAllSnippet> coupons;
    }

    public class  GetMyCouponResponseModel{
        public boolean success;
        public ArrayList<CouponMySnippet> coupons;
    }

    public class LoadCouponAllDetailResponseModel {
        public boolean success;
        public CouponAllDetailSnippet coupon_details;
    }
    public class LoadCouponMyDetailResponseModel {
        public boolean success;
        public CouponMyDetailSnippet coupon_code_details;
    }

    public class BuyouponDetailResponseModel{
        public boolean success;
        public String coupon_code;

    }

    public class RedeemCouponDetailResponseModel{
        public int error;

    }

    public class ReportCouponResponseModel{
        public int error;

    }

    public class CouponPostResponseModel {
        public int error;
        public boolean success;
        public String message;
        public Double wallet_balance;
        public Double amount_credited;
    }

    public static class GetWalletDetailsResponseModel {
        public boolean success;

        public WalletDetail wallet;

        public class WalletDetail {
            public int w_id;
            public String owner_id;
            public String owner_type;
            public Double balance;
            public ArrayList<UsageEntry> wallet_usage;
        }

        public class UsageEntry {
            public String urid_type;
            public int usage_type;
            public double usage_number;
            public double usage_cap;
        }
    }

    public class GetAllTransactionsResponseModel {
        public boolean success;
        public String description;
        public String message;
        public ArrayList<AccountTransactionsDetailSnippet> transactions;
    }

    public class SendNumberToserverResponseModel {
        public int error;
    }

    public class GetUserProfileResponseModel {
        public int error;

        public UserProfile user_profile_details;

        public class UserProfile {
            public String user_email;
            public String user_first_name;
            public String user_last_name;
        }
    }

    public class LoadUserProfileResponseModel {
        public int error;
    }

    public class CouponsClicksResponseModel {
        public boolean success;
    }
    public class CouponsViewsResponseModel {
        public boolean success;
    }

    public class SignInResponseModel {
        public boolean success;
        public String user_code;

    }

    public class SignUpResponseModel {
        public boolean success;
        public String user_code;
        public boolean number_verified;

    }

    public class GetCityResponseModel {
        public boolean success;
        public ArrayList<CitySnippet> cities;
    }
}
