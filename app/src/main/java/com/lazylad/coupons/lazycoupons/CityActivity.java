package com.lazylad.coupons.lazycoupons;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.Toast;

import com.lazylad.coupons.lazycoupons.Network.APIResonseModel;
import com.lazylad.coupons.lazycoupons.Network.APISuggestionsService;
import com.lazylad.coupons.lazycoupons.Snippets.CitySnippet;

import java.util.ArrayList;
import java.util.List;

import retrofit.RestAdapter;
import retrofit.RetrofitError;

/**
 * Created by Amud on 31/01/16.
 */

public class CityActivity extends ActionBarActivity {

    private ArrayList<CitySnippet> m_cityList;

    private String m_areaCodeSelected;
    private String m_cityCodeSelected;
    private String m_city_image_addSelected;
    private int m_city_image_flagSelected;

    private String m_old_cityCodeSelected;

    private String m_cityNameSelected;
    private String m_areaNameSelected;

    private Spinner m_cityListSpinner;
    private Spinner m_areaListSpinner;

    private ArrayList<String> m_cityNamesList;
    private ArrayList<String> m_areaImageAddress;
    private ArrayList<String> m_areaNamesList;
    private List m_servTypeListDetails2 = new ArrayList<String>();


    private SharedPreferences prefs_location;

    private Button m_startMainActivityButton;
    Boolean isInternetPresent = true;

    //private ServiceTypeAdapter m_servTypeAdapter;
    private GridView m_servTypeGridView;
    public static int flag = 0;
    public static final String TAG = "MyTag";
    RecyclerView recyclerView;
    private Toolbar toolbar;
    FrameLayout overLayView;
    Bundle bundle;
    boolean loadedFlag;
    SharedPreferences prefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.city_layout);
        bundle = new Bundle();
        loadedFlag = false;
        prefs = getSharedPreferences(
                "com.lazylad.coupons.lazycoupons", Context.MODE_PRIVATE);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Select City");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setPadding(0, getStatusBarHeight(), 0, 0);
        recyclerView = (RecyclerView) findViewById(R.id.city_list_view);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(CityActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        overLayView = (FrameLayout) findViewById(R.id.overLayView);

        prefs_location = this.getSharedPreferences(
                "technologies.angular.lazylad", Context.MODE_PRIVATE);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isInternetPresent) {
            if (!loadedFlag)
                loadCities();
        } else {
            Toast.makeText(CityActivity.this, "Sorry , Internet connection is not avaiable", Toast.LENGTH_SHORT).show();
        }
    }


    private void loadCities() {


        String user_code;
        SharedPreferences m_sharedPref = getSharedPreferences(
                "com.lazylad.coupons.lazycoupons", Context.MODE_PRIVATE);
        user_code = m_sharedPref.getString(EditorConstants.USER_CODE_CONSTANT, EditorConstants.DEFAULT_USER_CODE_CONSTANT);


        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://52.74.141.166").setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.getCitiesAPICall(user_code,new retrofit.Callback<APIResonseModel.GetCityResponseModel>() {

            @Override
            public void success(APIResonseModel.GetCityResponseModel getCityResponseModel, retrofit.client.Response response) {

                boolean success = getCityResponseModel.success;
                if (success) {
                    loadedFlag = true;
                    m_cityList = new ArrayList<CitySnippet>();
                    m_cityList = getCityResponseModel.cities;
                    recyclerView.setAdapter(new CityAdapter(m_cityList, R.layout.city_list_layout, CityActivity.this, 0));
                    progressDialog.dismiss();

                } else
                    Log.d("ser_ty_err", "service_type_from_server_error");
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                progressDialog.dismiss();
            }
        });
    }


    public void startCouponsActivity()
    {
        startActivity(new Intent(getApplicationContext(), (CouponClass.class)));
        this.finish();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        // overridePendingTransition(R.anim.anim_zoom_out_back,R.anim.anim_in);
        finish();
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }


}

