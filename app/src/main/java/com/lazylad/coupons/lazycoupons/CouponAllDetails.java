package com.lazylad.coupons.lazycoupons;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lazylad.coupons.lazycoupons.Network.APIRequestModel;
import com.lazylad.coupons.lazycoupons.Network.APIResonseModel;
import com.lazylad.coupons.lazycoupons.Network.APISuggestionsService;
import com.lazylad.coupons.lazycoupons.SyncAdapter.CouponClicksSycnAdapter;
import com.squareup.picasso.Picasso;

import retrofit.RestAdapter;
import retrofit.RetrofitError;

/**
 * Created by Amud on 25/01/16.
 */

public class CouponAllDetails extends ActionBarActivity {
    private Toolbar m_toolbar;
    private String m_couponId;
    private Button m_submitButton;
    private ImageView m_couponImageView;
    private TextView m_couponNameTV;
    private TextView m_shortDescTV;
    private TextView m_descTV;
    private TextView m_termsAndConditions;
    private String m_userCode;
    private boolean buyCouponFlag;
    private CouponAllDetailSnippet couponAllDetailSnippet;
    private SharedPreferences prefs_location;
    private int areaCode;
    private int cityCode;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        setContentView(R.layout.coupon_all_details_layout);
        m_toolbar = (Toolbar) findViewById(R.id.coupon_toolbar);
        m_toolbar.setPadding(0, getStatusBarHeight(), 0, 0);
        setSupportActionBar(m_toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        prefs_location = getSharedPreferences(
                "com.lazylad.coupons.lazycoupons", Context.MODE_PRIVATE);
        areaCode = 1;
        cityCode = prefs_location.getInt(EditorConstants.CITY_CODE_CONSTANT, EditorConstants.DEFAULT_CITY_CODE_CONSTANT);
        m_userCode = prefs_location.getString(EditorConstants.USER_CODE_CONSTANT, EditorConstants.DEFAULT_USER_CODE_CONSTANT);
        m_couponId = getIntent().getStringExtra("coupon_id");

        m_couponImageView = (ImageView) findViewById(R.id.coupon_image);
        m_couponNameTV = (TextView) findViewById(R.id.coupon_name);
        m_shortDescTV = (TextView) findViewById(R.id.short_desc);
        m_descTV = (TextView) findViewById(R.id.desc);
        m_submitButton = (Button) findViewById(R.id.submit);
        m_termsAndConditions=(TextView) findViewById(R.id.terms_and_conditions);

        m_couponNameTV.setVisibility(View.GONE);
        m_shortDescTV.setVisibility(View.GONE);
        m_descTV.setVisibility(View.GONE);
        m_submitButton.setVisibility(View.GONE);
        m_couponImageView.setVisibility(View.GONE);
        m_submitButton.setVisibility(View.GONE);

        buyCouponFlag = false;
        couponAllDetailSnippet = new CouponAllDetailSnippet();

        loadCouponDetail(m_couponId);

        m_submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buyCoupon(m_couponId);
            }
        });
    }

    private void loadCouponDetail(String coupon_id) {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://52.74.141.166").setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);


        apiSuggestionsService.loadCouponAllDetailAPICall(coupon_id, new retrofit.Callback<APIResonseModel.LoadCouponAllDetailResponseModel>() {

            @Override
            public void success(APIResonseModel.LoadCouponAllDetailResponseModel loadCouponAllDetailResponseModel, retrofit.client.Response response) {
                Log.d("success","success");

                boolean success = loadCouponAllDetailResponseModel.success;
                if (success) {

                    couponAllDetailSnippet = loadCouponAllDetailResponseModel.coupon_details;
                    if(couponAllDetailSnippet != null ) {
                        String coupon_name = couponAllDetailSnippet.name;
                        String short_desc = couponAllDetailSnippet.short_desc;
                        String desc = couponAllDetailSnippet.desc;
                        String terms = couponAllDetailSnippet.terms;
                        boolean image_flag = couponAllDetailSnippet.img_flag;
                        Display display = getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int width = size.x;

                        if (image_flag) {
                            String image_address = couponAllDetailSnippet.img_address;
                            {

                                if (image_address == null || image_address == "")
                                    image_address = "\"\"";
                                Picasso.with(CouponAllDetails.this)
                                        .load(image_address)
                                        .resize(width, 0)
                                        .placeholder(R.drawable.loading_big)
                                        .error(R.drawable.loading_big)
                                        .into(m_couponImageView);

                                m_couponImageView.setVisibility(View.VISIBLE);
                            }
                        }

                        m_couponNameTV.setText(coupon_name);
                        m_shortDescTV.setText(short_desc);
                        m_descTV.setText(desc);
                        m_termsAndConditions.setText(terms);
                        m_couponNameTV.setVisibility(View.VISIBLE);
                        m_shortDescTV.setVisibility(View.VISIBLE);
                        m_descTV.setVisibility(View.VISIBLE);
                        m_submitButton.setVisibility(View.VISIBLE);
                        m_submitButton.setVisibility(View.VISIBLE);
                        CouponClicksSycnAdapter.syncImmediately(CouponAllDetails.this);
                    }

                    progressDialog.dismiss();
                } else
                    Log.d("ser_ty_err", "service_type_from_server_error");
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                progressDialog.dismiss();
            }
        });
    }

    private void buyCoupon(String couponIdentifier) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://52.74.141.166").setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);

        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.BuyCouponDetailRequestModel buyouponDetailRequestModel = requestModel.new BuyCouponDetailRequestModel();
        buyouponDetailRequestModel.coupon_id = couponIdentifier;
        buyouponDetailRequestModel.user_code = m_userCode;
        buyouponDetailRequestModel.area_code = areaCode;
        buyouponDetailRequestModel.city_id = cityCode;
        apiSuggestionsService.buyCouponDetailAPICall(buyouponDetailRequestModel, new retrofit.Callback<APIResonseModel.BuyouponDetailResponseModel>() {

            @Override
            public void success(APIResonseModel.BuyouponDetailResponseModel buyouponDetailResponseModel, retrofit.client.Response response) {

                boolean success = buyouponDetailResponseModel.success;
                if (success) {
                    Toast.makeText(CouponAllDetails.this, "Coupon has been bought, Thanks", Toast.LENGTH_SHORT).show();
                    m_submitButton.setEnabled(false);
                    buyCouponFlag = true;
                    progressDialog.dismiss();
                    onBackPressed();
                } else {
                    progressDialog.dismiss();
                    Log.d("ser_ty_err", "service_type_from_server_error");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                progressDialog.dismiss();
            }
        });
    }


    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (buyCouponFlag) {
            Intent intent = new Intent(this, CouponClass.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("buyCouponFlag", buyCouponFlag);
            this.startActivity(intent);
            this.finish();
        }
    }
}
