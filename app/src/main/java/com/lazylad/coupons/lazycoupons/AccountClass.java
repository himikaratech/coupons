package com.lazylad.coupons.lazycoupons;

/**
 * Created by Amud on 28/09/15.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.astuetz.PagerSlidingTabStrip;
/**
 * Created by user on 27-07-2015.
 */
public class AccountClass extends ActionBarActivity {

    Toolbar toolbar;
    ViewPager pager;
    AccountsPagerAdapter adapter;
    PagerSlidingTabStrip tabs;
    CharSequence Titles[] = {"Summary", "Account"};
    int Numboftabs = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_layout);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Lazy Wallet");
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       // toolbar.setPadding(0, getStatusBarHeight(), 0, 0);
        adapter = new AccountsPagerAdapter(getSupportFragmentManager(), Titles, Numboftabs);
        pager = (ViewPager) findViewById(R.id.pager);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences prefs = this.getSharedPreferences(
                "com.lazylad.coupons.lazycoupons", Context.MODE_PRIVATE);
        Bundle bundle = new Bundle();
     /*   Utils.verify_num(this, bundle, 3, false, NumberParameters.WALLET_SKIP_CONSTANT, NumberParameters.WALLET_VERIFIED_CONSTANT);
        boolean prefVerified = prefs.getBoolean(NumberParameters.WALLET_VERIFIED_CONSTANT, false);
       // boolean prefVerified = true;
        Log.i("preverifiedFlag", prefVerified+"");
        if (prefVerified) {*/
            pager.setAdapter(adapter);
            tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
            tabs.setShouldExpand(true);

            // Setting the ViewPager For the SlidingTabsLayout
            tabs.setViewPager(pager);

            pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(int position) {
                    // on changing the page
                    // make respected tab selected
                    tabs.setViewPager(pager);
                }

                @Override
                public void onPageScrolled(int arg0, float arg1, int arg2) {
                }

                @Override
                public void onPageScrollStateChanged(int arg0) {
                }
            });
        }

    @Override
    protected void onPause() {
        super.onPause();
    }
}