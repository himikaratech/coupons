package com.lazylad.coupons.lazycoupons;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Amud on 30/09/15.
 */
public class AccountTransDetailAdap extends RecyclerView.Adapter<AccountTransDetailAdap.ViewHolder> {

    private ArrayList<AccountTransactionsDetailSnippet> m_accountTransactionsDetail;
    private Context m_context;
    int viewFlag = 0;

    public AccountTransDetailAdap(Context context, ArrayList<AccountTransactionsDetailSnippet> accountTransactionsDetail) {
        this.m_context = context;
        this.m_accountTransactionsDetail = accountTransactionsDetail;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.acc_trans_detail_layout, parent, false);
        return new ViewHolder(v, viewFlag);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        if (m_accountTransactionsDetail != null) {

            AccountTransactionsDetailSnippet accountTransactionsDetailSnippet = new AccountTransactionsDetailSnippet();
            accountTransactionsDetailSnippet = m_accountTransactionsDetail.get(position);

            String t_id = accountTransactionsDetailSnippet.t_id;
            String desc = accountTransactionsDetailSnippet.description;
            String CorD = accountTransactionsDetailSnippet.CorD;
            Double amount = accountTransactionsDetailSnippet.amount;
            String status = accountTransactionsDetailSnippet.status;
            Long timeStamp = accountTransactionsDetailSnippet.time_transaction;

            Date date = new Date((timeStamp * 1000L));
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            format.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
            String timeOfTransaction = format.format(date);

            holder.t_idTextView.setText(t_id);
            holder.descTextView.setText(desc);

            holder.statusTextView.setText(status);
            holder.timeStampTextView.setText(timeOfTransaction);
            holder.cordTextView.setText(CorD);

            if (CorD.equals("Credit")) {
                holder.amountTextView.setTextColor(Color.parseColor("#78bd1e"));
                holder.cordTextView.setTextColor(Color.parseColor("#78bd1e"));
                holder.amountTextView.setText("+ ₹ " + String.valueOf(amount));
                holder.cordTextView.setText(CorD);
            } else if (CorD.equals("Debit")) {
                holder.amountTextView.setTextColor(Color.parseColor("#ff4f4f"));
                holder.cordTextView.setTextColor(Color.parseColor("#ff4f4f"));
                holder.amountTextView.setText("- ₹ " + String.valueOf(amount));
                holder.cordTextView.setText(CorD);
            }
        }
    }

    @Override
    public int getItemCount() {
        return m_accountTransactionsDetail.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView t_idTextView;
        TextView descTextView;
        TextView amountTextView;
        TextView statusTextView;
        TextView timeStampTextView;
        TextView cordTextView;

        public ViewHolder(View itemView, int flag) {
            super(itemView);

            t_idTextView = (TextView) itemView.findViewById(R.id.t_idTV);
            descTextView = (TextView) itemView.findViewById(R.id.descTV);
            amountTextView = (TextView) itemView.findViewById(R.id.amountTV);
            statusTextView = (TextView) itemView.findViewById(R.id.statusTV);
            timeStampTextView = (TextView) itemView.findViewById(R.id.timeStampTV);
            cordTextView = (TextView) itemView.findViewById(R.id.cord_texview);
        }
    }

    public Object getItem(int position) {
        if (m_accountTransactionsDetail != null) {
            return m_accountTransactionsDetail.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
