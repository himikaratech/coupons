package com.lazylad.coupons.lazycoupons;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.lazylad.coupons.lazycoupons.Network.APIRequestModel;
import com.lazylad.coupons.lazycoupons.Network.APIResonseModel;
import com.lazylad.coupons.lazycoupons.Network.APISuggestionsService;

import retrofit.RestAdapter;
import retrofit.RetrofitError;

/**
 * Created by Amud on 25/01/16.
 */

public class CouponReport extends ActionBarActivity {
    private Toolbar m_toolbar;

    private Button m_submitButton;
    private TextView m_reportTextView;
    private int m_userCode;
    private String m_couponIdentifier;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        setContentView(R.layout.coupon_report_layout);
        m_toolbar = (Toolbar) findViewById(R.id.coupon_toolbar);
        m_toolbar.setPadding(0, getStatusBarHeight(), 0, 0);
        setSupportActionBar(m_toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        m_userCode=getIntent().getIntExtra("m_userCode", 0);
        m_couponIdentifier=getIntent().getStringExtra("couponIdentifier");
        m_submitButton=(Button)findViewById(R.id.submit);
        m_reportTextView=(TextView)findViewById(R.id.details);


        m_submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reportCoupon(m_couponIdentifier);
            }
        });
    }

    private void reportCoupon(String couponIdentifier ) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://www.angulartechnologies.com").setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);

        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.ReportCouponCouponDetailRequestModel reportCouponCouponDetailRequestModel = requestModel.new ReportCouponCouponDetailRequestModel();
        reportCouponCouponDetailRequestModel.bought_id = couponIdentifier;
        reportCouponCouponDetailRequestModel.user_code=m_userCode;
        reportCouponCouponDetailRequestModel.report_stmt=m_reportTextView.getText().toString();

        apiSuggestionsService.reportCouponAPICall(reportCouponCouponDetailRequestModel, new retrofit.Callback<APIResonseModel.ReportCouponResponseModel>() {

            @Override
            public void success(APIResonseModel.ReportCouponResponseModel reportCouponResponseModel, retrofit.client.Response response) {

                int success = reportCouponResponseModel.error;
                if (success == 1) {
                    Toast.makeText(CouponReport.this, "Coupon has been reported, Thanks", Toast.LENGTH_SHORT).show();
                    m_submitButton.setEnabled(false);
                    progressDialog.dismiss();
                    onBackPressed();
                } else
                    Log.d("ser_ty_err", "service_type_from_server_error");
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                progressDialog.dismiss();

            }
        });
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}

