package com.lazylad.coupons.lazycoupons;

/**
 * Created by Amud on 28/09/15.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lazylad.coupons.lazycoupons.Network.APIRequestModel;
import com.lazylad.coupons.lazycoupons.Network.APIResonseModel;
import com.lazylad.coupons.lazycoupons.Network.APISuggestionsService;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AccountsDetailFrag extends Fragment {

    protected String m_userCode;
    String m_number;
    ArrayList<AccountTransactionsDetailSnippet> m_walletList;
    RecyclerView recyclerView;
    AccountTransDetailAdap accountTransDetailAdap;
    TextView m_noTransactions;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.account_detail_frag, container, false);
        recyclerView = (RecyclerView) v.findViewById(R.id.transation_list_view);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        m_noTransactions = (TextView) v.findViewById(R.id.noTransactions);
        m_noTransactions.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        getAllTransactions();
        return v;
    }

    private void getAllTransactions() {
        SharedPreferences m_sharedPref = getContext().getSharedPreferences(
                "com.lazylad.coupons.lazycoupons", Context.MODE_PRIVATE);
        m_userCode = m_sharedPref.getString(EditorConstants.USER_CODE_CONSTANT, EditorConstants.DEFAULT_USER_CODE_CONSTANT);


        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.GetAllTransactionsRequestModel getAllTransactionsRequestModel = requestModel.new GetAllTransactionsRequestModel();
        getAllTransactionsRequestModel.user_code = String.valueOf(m_userCode);
        getAllTransactionsRequestModel.owner_type = 0;
        getAllTransactionsRequestModel.previous_tid = 0;

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://www.angulartechnologies.com")
                .build();

        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        final ProgressDialog progressDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        Log.e("detailObject", getAllTransactionsRequestModel.toString());

        apiSuggestionsService.getAllTransactionsAPICall(getAllTransactionsRequestModel.user_code
                , getAllTransactionsRequestModel.owner_type,
                new Callback<APIResonseModel.GetAllTransactionsResponseModel>() {


                    @Override
                    public void success(APIResonseModel.GetAllTransactionsResponseModel getAllTransactionsResponseModel, Response response) {
                        Boolean success = getAllTransactionsResponseModel.success;
                        Log.e("Detailsuccess", success + "");
                        if (progressDialog.isShowing())
                            progressDialog.dismiss();

                        if (success == true) {
                            m_walletList = new ArrayList<AccountTransactionsDetailSnippet>();
                            m_walletList = getAllTransactionsResponseModel.transactions;
                            if (!m_walletList.isEmpty()) {
                                accountTransDetailAdap = new AccountTransDetailAdap(getActivity(), m_walletList);
                                recyclerView.setVisibility(View.VISIBLE);
                                recyclerView.setAdapter(accountTransDetailAdap);
                            } else {
                                m_noTransactions.setVisibility(View.VISIBLE);
                            }

                            if (progressDialog.isShowing())
                                progressDialog.dismiss();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.e("GetAllTransationRetro", error.toString());
                        if (progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                });
    }
}