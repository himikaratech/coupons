package com.lazylad.coupons.lazycoupons;

        import android.app.ProgressDialog;
        import android.content.Context;
        import android.content.SharedPreferences;
        import android.database.Cursor;
        import android.graphics.Point;
        import android.net.ConnectivityManager;
        import android.net.NetworkInfo;
        import android.os.Bundle;
        import android.support.v4.view.ViewPager;
        import android.support.v4.widget.DrawerLayout;
        import android.support.v7.app.ActionBarActivity;
        import android.support.v7.app.ActionBarDrawerToggle;
        import android.support.v7.widget.LinearLayoutManager;
        import android.support.v7.widget.RecyclerView;
        import android.support.v7.widget.Toolbar;
        import android.util.Log;
        import android.view.Display;
        import android.view.Gravity;
        import android.view.View;
        import android.view.ViewGroup;

        import com.astuetz.PagerSlidingTabStrip;
        import com.lazylad.coupons.lazycoupons.Network.APIRequestModel;
        import com.lazylad.coupons.lazycoupons.Network.APIResonseModel;
        import com.lazylad.coupons.lazycoupons.Network.APISuggestionsService;

        import java.util.ArrayList;

        import retrofit.RestAdapter;
        import retrofit.RetrofitError;

/**
 * Created by Amud on 18/09/15.
 */
public class CouponClass extends ActionBarActivity {

    public ArrayList<String> m_couponTabNames;
    public  ArrayList<Integer> m_couponStrips;
    private ViewPager m_viewPager;
    private Toolbar m_toolbar;
    private CouponPagerAdapter couponPagerAdapter ;
    private PagerSlidingTabStrip tabs;

    String Name;
    public static String mAddressOutput = "LazyLad Coupon";
    private Boolean m_buyCouponFlag;

    Bundle bundle;
    protected String m_userCode;
    int loadAddressFlag=0;

    String TITLES[] = {"Home"};
    int ICONS[] = {R.drawable.home};

    RecyclerView mRecyclerView;                           // Declaring RecyclerView
    RecyclerView.Adapter mAdapter;                        // Declaring Adapter For Recycler View
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    DrawerLayout Drawer;                                  // Declaring DrawerLayout
    public ActionBarDrawerToggle mDrawerToggle;
    private View frame;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        setContentView(R.layout.coupon_layout);
        bundle= new Bundle();
        m_buyCouponFlag=getIntent().getBooleanExtra("buyCouponFlag",false);
        m_toolbar = (Toolbar) findViewById(R.id.coupon_toolbar);
        m_toolbar.setPadding(0, getStatusBarHeight(), 0, 0);
        setSupportActionBar(m_toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        SharedPreferences m_sharedPref = getSharedPreferences(
                "com.lazylad.coupons.lazycoupons.SyncAdapter", Context.MODE_PRIVATE);
        m_userCode = m_sharedPref.getString(EditorConstants.USER_CODE_CONSTANT, EditorConstants.DEFAULT_USER_CODE_CONSTANT);


        tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setVisibility(View.GONE);
        m_couponStrips=new ArrayList<Integer>();
        m_couponTabNames=new ArrayList<String>();
        m_couponStrips.add(1);
        m_couponStrips.add(2);
        m_couponTabNames.add(getResources().getString(R.string.all_coupon));
        m_couponTabNames.add(getResources().getString(R.string.your_coupon));

        mRecyclerView = (RecyclerView) findViewById(R.id.nav_drawer_RecyclerView); // Assigning the RecyclerView Object to the xml View
        // mRecyclerView.setHasFixedSize(true);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int displayViewWidth_ = size.x;
        frame = (View) findViewById(R.id.content_frame);

        mAdapter = new MyAdapter(TITLES, ICONS, Name, mAddressOutput, CouponClass.this, 1);// Creating the Adapter of MyAdapter class(which we are going to see in a bit)
        mRecyclerView.setAdapter(mAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout); // Drawer object Assigned to the view

        ViewGroup.LayoutParams params = mRecyclerView.getLayoutParams();
        params.width = 9 * (displayViewWidth_ / 10);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutParams(params);

        mDrawerToggle = new ActionBarDrawerToggle(this, Drawer, m_toolbar, R.string.opendrawer, R.string.closedrawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        }; // Drawer Toggle Object Made
        Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
        mDrawerToggle.syncState(); // Finally we set the drawer toggle sync State

        makeStrip();

    }



    void makeStrip()
    {
        m_viewPager=(ViewPager)findViewById(R.id.coupon_pager);

        couponPagerAdapter= new CouponPagerAdapter(getSupportFragmentManager(),m_couponStrips,this);

        m_viewPager.setAdapter(couponPagerAdapter);
        setSupportActionBar(m_toolbar);
        m_toolbar.setVisibility(View.VISIBLE);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        tabs.setVisibility(View.VISIBLE);
        tabs.setShouldExpand(true);
        tabs.setDividerColor(getResources().getColor(R.color.line_background));
        if(m_buyCouponFlag)
            m_viewPager.setCurrentItem(1);
        tabs.setViewPager(m_viewPager);

        m_viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                // on changing the page
                // make respected tab selected
                tabs.setViewPager(m_viewPager);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
      /*  Bundle bundle = new Bundle();
        Utils.verify_num(this, bundle, 1, true, NumberParameters.HOME_SKIP_CONSTANT, NumberParameters.HOME_VERIFIED_CONSTANT);
   */ }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }
    @Override
    public void onStop() {
        super.onStop();
        Drawer.closeDrawer(Gravity.LEFT);
    }
}

