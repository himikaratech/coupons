package com.lazylad.coupons.lazycoupons;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

/**
 * Created by Amud on 14/09/15.
 */
public class LazyCouponProvider extends ContentProvider {

    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private DBHelper dBHelper;

    private static final int USER_DETAILS = 100;
    private static final int COUPONS_CLICKS = 101;
    private static final int COUPONS_VIEWS = 102;

    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = LazyCouponContract.CONTENT_AUTHORITY;

        matcher.addURI(authority, LazyCouponContract.PATH_USER_DETAILS, USER_DETAILS);
        matcher.addURI(authority, LazyCouponContract.PATH_COUPONS_CLICKS, COUPONS_CLICKS);
        matcher.addURI(authority, LazyCouponContract.PATH_COUPONS_VIEWS, COUPONS_VIEWS);
        return matcher;
    }

    @Override
    public boolean onCreate() {

        dBHelper = new DBHelper(getContext());
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor retCursor;
        int match = sUriMatcher.match(uri);
        switch (match) {

            case USER_DETAILS: {
                retCursor = dBHelper.getReadableDatabase().query(
                        LazyCouponContract.UserDetailsEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case COUPONS_VIEWS: {
                retCursor = dBHelper.getReadableDatabase().query(
                        LazyCouponContract.CouponsViewsDetails.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            case COUPONS_CLICKS: {
                retCursor = dBHelper.getReadableDatabase().query(
                        LazyCouponContract.CouponsClicksDetails.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);

        return retCursor;
    }

    @Override
    public String getType(Uri uri) {

        // Use the Uri Matcher to determine what kind of URI this is.
        final int match = sUriMatcher.match(uri);

        switch (match) {
            case USER_DETAILS:
                return LazyCouponContract.UserDetailsEntry.CONTENT_TYPE;
            case COUPONS_CLICKS:
                return LazyCouponContract.CouponsClicksDetails.CONTENT_TYPE;
            case COUPONS_VIEWS:
                return LazyCouponContract.CouponsViewsDetails.CONTENT_TYPE;
             default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {

        final SQLiteDatabase db = dBHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Uri returnUri;

        switch (match) {
            case USER_DETAILS: {
                long _id = db.insert(LazyCouponContract.UserDetailsEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = LazyCouponContract.UserDetailsEntry.buildUserDetailsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case COUPONS_VIEWS: {
                long _id = db.insert(LazyCouponContract.CouponsViewsDetails.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = LazyCouponContract.CouponsViewsDetails.buildCouponsViewsUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case COUPONS_CLICKS: {
                long _id = db.insert(LazyCouponContract.CouponsClicksDetails.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = LazyCouponContract.CouponsClicksDetails.buildCouponsClicksUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {

        final SQLiteDatabase db = dBHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsDeleted;
        switch (match) {
            case USER_DETAILS:
                rowsDeleted = db.delete(
                        LazyCouponContract.UserDetailsEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case COUPONS_CLICKS:
                rowsDeleted = db.delete(
                        LazyCouponContract.CouponsClicksDetails.TABLE_NAME, selection, selectionArgs);
                break;
            case COUPONS_VIEWS:
                rowsDeleted = db.delete(
                        LazyCouponContract.CouponsViewsDetails.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {

        final SQLiteDatabase db = dBHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int rowsUpdated;

        switch (match) {
            case USER_DETAILS:
                rowsUpdated = db.update(LazyCouponContract.UserDetailsEntry.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case COUPONS_VIEWS:
                rowsUpdated = db.update(LazyCouponContract.CouponsViewsDetails.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
            case COUPONS_CLICKS:
                rowsUpdated = db.update(LazyCouponContract.CouponsClicksDetails.TABLE_NAME, values, selection,
                        selectionArgs);
                break;
              default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsUpdated;
    }
}
