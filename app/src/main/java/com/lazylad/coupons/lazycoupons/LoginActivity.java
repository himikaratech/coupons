package com.lazylad.coupons.lazycoupons;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.lazylad.coupons.lazycoupons.Network.APIRequestModel;
import com.lazylad.coupons.lazycoupons.Network.APIResonseModel;
import com.lazylad.coupons.lazycoupons.Network.APISuggestionsService;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Amud on 31/01/16.
 */
public class LoginActivity extends Activity {


    private EditText m_phoneNumerEditText;


    private String m_phoneNumber;

    private Button m_signInButton;

    private TextView m_title;
    private TextView m_details;
    private TextView m_endNote;
    SharedPreferences prefs;


    private String m_servProvId;
    private String m_servProvType;
    private Long m_sp_exp_del_time;
    private String regid;
    int count = 0;
    static final String TAG = "GCMDemo";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private static String SENDER_ID = "495574458613";
    private AtomicInteger msgId = new AtomicInteger();

    private Context context;

    private static String[] dummy = new String[]{"a", "b"};
    private static Uri insertUri;

    static {
        LoginActivity.insertUri = Uri.parse((String) ("content://technologies.angular.lazyladserviceprovider.lazyladserviceprovider.provider/INSERT"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        prefs = this.getSharedPreferences(
                "com.lazylad.coupons.lazycoupons", Context.MODE_PRIVATE);


          m_phoneNumerEditText = (EditText) findViewById(R.id.phone_number_editview);


        m_signInButton = (Button) findViewById(R.id.submit);
        m_servProvId = "0";
        m_servProvType = "0";
        context = getApplicationContext();


        m_signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 m_phoneNumber = m_phoneNumerEditText.getText().toString();

                if ( m_phoneNumber == null || m_phoneNumber.isEmpty() || m_phoneNumber.length() < 10) {
                    Toast.makeText(getApplicationContext(), "Please enter your valid Phone Number", Toast.LENGTH_SHORT).show();
                } else {
                    SignInServProv();
                }
            }
        });

    }


    private void SignInServProv() {


        final ProgressDialog pDialog;

        pDialog = new ProgressDialog(LoginActivity.this);
        pDialog.setMessage("Signing In. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://52.74.141.166").setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.LogInRequestModel logInRequestModel = requestModel.new LogInRequestModel();

        logInRequestModel.phone_number = m_phoneNumber;


        apiSuggestionsService.signInAPICall( logInRequestModel, new Callback<APIResonseModel.SignInResponseModel>() {

            @Override
            public void success(APIResonseModel.SignInResponseModel signInResponseModel, Response response) {

                boolean success = signInResponseModel.success;
                Log.i("success value", success + "");
                if (success == true) {
                    String user_code = signInResponseModel.user_code;

                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString(EditorConstants.USER_CODE_CONSTANT, user_code);
                    editor.commit();
                    startActivity(new Intent(getApplicationContext(), (CityActivity.class)));
                    LoginActivity.this.finish();

                }
                if (pDialog.isShowing())
                    pDialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("RetrofitError", error.toString());
                if (pDialog.isShowing())
                    pDialog.dismiss();
            }
        });
    }


    public void call_us(View v) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:+91-8011139511"));
        startActivity(callIntent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
