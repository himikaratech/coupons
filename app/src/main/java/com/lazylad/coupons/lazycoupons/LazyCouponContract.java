package com.lazylad.coupons.lazycoupons;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

public class LazyCouponContract {


    public static final String CONTENT_AUTHORITY = "com.lazylad.coupons.lazycoupons.provider";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_USER_DETAILS = "user_details";
    public static final String PATH_COUPONS_VIEWS = "coupons_views";
    public static final String PATH_COUPONS_CLICKS = "coupons_clicks";

    public static final class UserDetailsEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_USER_DETAILS).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_USER_DETAILS;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_USER_DETAILS;
        public static final String TABLE_NAME = "user_details";

        public static final String COLUMN_USER_CODE = "user_code";
        public static final String COLUMN_VERIFIED_FLAG = "varified_flag";
        public static final String COLUMN_VERIFIED_NUMBER = "varified_number";
        public static final String COLUMN_EMAIL_FLAG = "email_flag";
        public static final String COLUMN_EMAIL_ID = "email_id";

        public static Uri buildUserDetailsUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class CouponsStatistics implements BaseColumns {

        public static final String COLUMN_COUPON_ID = "coupon_id";
        public static final String COLUMN_COUPON_UP_SYNC = "coupon_up_sync";
        public static final String COLUMN_TIME_STAMP= "time_stamp";

         }

    public static final class CouponsViewsDetails implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_COUPONS_VIEWS).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_COUPONS_VIEWS;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_COUPONS_VIEWS;
        public static final String TABLE_NAME = "coupons_views";

        public static final String COLUMN_VIEWS_COUNT = "views_count";

        public static Uri buildCouponsViewsUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    public static final class CouponsClicksDetails implements BaseColumns {

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_COUPONS_CLICKS).build();

        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_COUPONS_CLICKS;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_COUPONS_CLICKS;
        public static final String TABLE_NAME = "coupons_clicks";

        public static final String COLUMN_CLICKS_COUNT = "views_count";

        public static Uri buildCouponsClicksUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }
}
