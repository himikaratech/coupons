package com.lazylad.coupons.lazycoupons;

/**
 * Created by Amud on 28/09/15.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class AccountsPagerAdapter extends FragmentStatePagerAdapter {

    CharSequence Titles[];
    int NumbOfTabs;

    public AccountsPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
        super(fm);

        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;
    }

    @Override
    public Fragment getItem(int position) {

        if (position == 0) {
            AccountSummaryFrag accountSummaryFrag = new AccountSummaryFrag();
            return accountSummaryFrag;
        } else {
            AccountsDetailFrag accountsDetailFrag = new AccountsDetailFrag();
            return accountsDetailFrag;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    @Override
    public int getCount() {
        return NumbOfTabs;
    }
}