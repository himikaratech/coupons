package com.lazylad.coupons.lazycoupons;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;

import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

/**
 * Created by sakshigupta on 27/01/16.
 */
public class Utils {

    public static void verify_num(Context context, Bundle dataIntent, int situation, boolean skippable, String prefSkippedString, String prefVerifiedString) {
        SharedPreferences prefs = context.getSharedPreferences(
                "com.lazylad.coupons.lazycoupons", Context.MODE_PRIVATE);
        boolean skipped = prefs.getBoolean(prefSkippedString, false);
        int varified_flag_pref = prefs.getInt(EditorConstants.VARIFIED_FLAG_PREF_CONSTANT, 0);
        int email_verify_flag = prefs.getInt(EditorConstants.VARIFIED_EMAIL_FLAG_PREF_CONSTANT, 0);
        int shallverfiy_flag_pref = prefs.getInt(EditorConstants.SHALL_NUMBER_VERIFY_CONSTANT, 1);
        Log.i("shall verify", shallverfiy_flag_pref+"");
        Log.i("skipped", skipped+"");
        Log.i("verified flag", varified_flag_pref+"");
        if (shallverfiy_flag_pref == 1 && skipped == false) {

            if (varified_flag_pref == 0) {
                Intent intent = new Intent();
                NumberClass.setContext(context);
                intent.putExtra(NumberParameters.SITUATION_CONTANT, situation);
                intent.putExtra(NumberParameters.SKIPPABLE_CONSTANT, skippable);
                intent.putExtra(NumberParameters.PREF_STRING_CONTANT, prefSkippedString);
                intent.putExtra(NumberParameters.PREF_VERIFIED_CONSTANT, prefVerifiedString);
                intent.putExtras(dataIntent);
                intent.setClass(context, NumberClass.class);
                if (!skippable)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            } else {
                Utils.verify_email(context, dataIntent, skippable, prefSkippedString, prefVerifiedString);

            }
        }
    }

    public static void verify_email(Context context, Bundle dataIntent, boolean skippable, String prefSkippedString, String prefVerifiedString) {
        int varified_flag = 0;

        Cursor m_cursor = context.getContentResolver().query(
                LazyCouponContract.UserDetailsEntry.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                varified_flag = m_cursor.getInt(4);
                if (varified_flag == 0) {
                    Log.d("verify","verify");
                    Intent intent = new Intent();
                    EmailVerify.setContext(context);
                    intent.putExtras(dataIntent);
                    intent.putExtra(NumberParameters.PREF_VERIFIED_CONSTANT, prefVerifiedString);
                    intent.putExtra(NumberParameters.PREF_STRING_CONTANT, prefSkippedString);
                    intent.putExtra(NumberParameters.SKIPPABLE_CONSTANT, skippable);
                    intent.setClass(context, EmailVerify.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(intent);
                } else {
                    SharedPreferences prefs = context.getSharedPreferences(
                            "com.lazylad.coupons.lazycoupons", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putBoolean(prefVerifiedString, true);
                    editor.commit();
                }
            }
        }

        else{
            Log.i("No data in local db", "Yes");
        }
    }

    public static void ShowMessageBox(AlertDialog.Builder dialogBuilder, String title, String message) {
        Utils.ShowMessageBox(dialogBuilder, title, message, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }

    public static void ShowMessageBox(AlertDialog.Builder dialogBuilder, String title, String message, DialogInterface.OnClickListener listener) {
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Close", listener);

        if (!alertDialog.isShowing())
            alertDialog.show();
    }

    public static void TrustInvalidSslCertificates() {
        try {
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, new X509TrustManager[]{new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            }}, new SecureRandom());

            HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        NetworkInfo networkInfo = (NetworkInfo) ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if ((networkInfo == null) || (!(networkInfo.isConnected()))) return false;
        return true;
    }
}
