package com.lazylad.coupons.lazycoupons;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.lazylad.coupons.lazycoupons.Network.APIRequestModel;
import com.lazylad.coupons.lazycoupons.Network.APIResonseModel;
import com.lazylad.coupons.lazycoupons.Network.APISuggestionsService;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit.RestAdapter;
import retrofit.RetrofitError;

public class EmailVerify extends Activity {

    TextView m_emailTV;
    TextView m_firstNameTV;
    TextView m_lastNameTV;
    Button submit;
    String m_changed_email_id;
    String m_changed_fisrtName;
    String m_changed_lastName;

    String m_old_email_id;
    String m_old_fisrtName;
    String m_old_lastName;

    static Context mContext;
    private int m_userCode = 0;
    private int email_flag = 0;
    SharedPreferences prefs;
    String prefVerifiedString;
    String prefSkippedString;
    Boolean skippable;

    private Pattern pattern;
    private Matcher matcher;

    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public EmailVerify() {
        pattern = Pattern.compile(EMAIL_PATTERN);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile);
        prefVerifiedString = getIntent().getStringExtra(NumberParameters.PREF_VERIFIED_CONSTANT);
        prefSkippedString = getIntent().getStringExtra(NumberParameters.PREF_STRING_CONTANT);
        skippable = getIntent().getBooleanExtra(NumberParameters.SKIPPABLE_CONSTANT, false);
        m_emailTV = (TextView) findViewById(R.id.customer_email);
        m_firstNameTV = (TextView) findViewById(R.id.customer_fist_name);
        m_lastNameTV = (TextView) findViewById(R.id.customer_last_name);

        m_emailTV.setVisibility(View.GONE);
        m_firstNameTV.setVisibility(View.GONE);
        m_lastNameTV.setVisibility(View.GONE);
        submit = (Button) findViewById(R.id.submitButton);
        submit.setVisibility(View.GONE);

        m_changed_email_id = new String();
        m_changed_fisrtName = new String();
        m_changed_lastName = new String();

        m_old_email_id = new String();
        m_old_fisrtName = new String();
        m_old_lastName = new String();

        Cursor m_cursor = getContentResolver().query(
                LazyCouponContract.UserDetailsEntry.CONTENT_URI,
                null,
                null,
                null,
                null);
        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                m_userCode = m_cursor.getInt(1);
                email_flag = m_cursor.getInt(2);
            }
        } else {
            m_userCode = 0;
            email_flag = 0;
        }

        getUserProfileDetails(m_userCode);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                m_changed_email_id = m_emailTV.getText().toString().trim();
                m_changed_fisrtName = m_firstNameTV.getText().toString().trim();
                m_changed_lastName = m_lastNameTV.getText().toString().trim();

                if (m_changed_fisrtName.isEmpty() || m_changed_fisrtName == null) {
                    Toast.makeText(EmailVerify.this, "Please enter Fist Name", Toast.LENGTH_LONG).show();
                } else if (validate(m_changed_email_id)) {
                    sendEmailToServer(m_userCode, m_changed_email_id, m_changed_fisrtName, m_changed_lastName);
                } else
                    Toast.makeText(EmailVerify.this, "Please enter a valid Email ID", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getUserProfileDetails(int userCode) {
        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.GetUserProfileRequestModel getUserProfileRequestModel = requestModel.new GetUserProfileRequestModel();
        getUserProfileRequestModel.user_code = Integer.toString(userCode);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://www.angulartechnologies.com")
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.getUserProfileAPICall(getUserProfileRequestModel, new retrofit.Callback<APIResonseModel.GetUserProfileResponseModel>() {

            @Override
            public void success(APIResonseModel.GetUserProfileResponseModel sendEmailToServerResponseModel, retrofit.client.Response response) {
                int success = sendEmailToServerResponseModel.error;
                if (success == 1) {
                    m_old_email_id = sendEmailToServerResponseModel.user_profile_details.user_email;
                    m_old_fisrtName = sendEmailToServerResponseModel.user_profile_details.user_first_name;
                    m_old_lastName = sendEmailToServerResponseModel.user_profile_details.user_last_name;

                    submit.setVisibility(View.VISIBLE);
                    m_emailTV.setVisibility(View.VISIBLE);
                    m_firstNameTV.setVisibility(View.VISIBLE);
                    m_lastNameTV.setVisibility(View.VISIBLE);

                    m_emailTV.setText(m_old_email_id);
                    m_firstNameTV.setText(m_old_fisrtName);
                    m_lastNameTV.setText(m_old_lastName);
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                Toast.makeText(EmailVerify.this, R.string.network_error, Toast.LENGTH_SHORT);
                progressDialog.dismiss();
            }
        });
    }

    private void sendEmailToServer(int userCode, String changed_email_id, String changed_fisrtName, String changed_lastName) {
        final ProgressDialog progressDialog = new ProgressDialog(this, R.style.MyTheme);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.LoudUserProfileRequestModel sendEmailToServerRequestModel = requestModel.new LoudUserProfileRequestModel();

        sendEmailToServerRequestModel.user_code = Integer.toString(userCode);

        if (!changed_email_id.equals(m_old_email_id))
            sendEmailToServerRequestModel.user_email = m_changed_email_id;
        if (!changed_fisrtName.equals(m_old_fisrtName))
            sendEmailToServerRequestModel.user_first_name = m_changed_fisrtName;
        if (!changed_lastName.equals(m_old_lastName))
            sendEmailToServerRequestModel.user_last_name = m_changed_lastName;

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://www.angulartechnologies.com")
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);
        apiSuggestionsService.loadUserProfileAPICall(sendEmailToServerRequestModel, new retrofit.Callback<APIResonseModel.LoadUserProfileResponseModel>() {

            @Override
            public void success(APIResonseModel.LoadUserProfileResponseModel sendEmailToServerResponseModel, retrofit.client.Response response) {
                int success = sendEmailToServerResponseModel.error;
                if (success == 1) {
                    ContentValues values = new ContentValues();
                    values.put(LazyCouponContract.UserDetailsEntry.COLUMN_EMAIL_FLAG, 1);
                    values.put(LazyCouponContract.UserDetailsEntry.COLUMN_EMAIL_ID, m_emailTV.getText().toString());
                    getContentResolver().update(LazyCouponContract.UserDetailsEntry.CONTENT_URI, values, LazyCouponContract.UserDetailsEntry.COLUMN_USER_CODE + "=?", new String[]{String.valueOf(m_userCode)});

                    prefs = EmailVerify.this.getSharedPreferences(
                            "com.lazylad.coupons.lazycoupons", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    if (skippable == true) {
                        editor.putBoolean(prefSkippedString, true);
                        editor.putBoolean(prefVerifiedString, true);
                    }
                    editor.putInt(EditorConstants.VARIFIED_EMAIL_FLAG_PREF_CONSTANT, 1);
                    editor.putBoolean(prefVerifiedString, true);
                    editor.commit();
                    progressDialog.dismiss();

                    Bundle bundle = new Bundle();
                    bundle = getIntent().getExtras();
                    Intent in = new Intent(EmailVerify.this, mContext.getClass());
                    in.putExtras(bundle);

                    startActivity(in);
                    EmailVerify.this.finish();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                Toast.makeText(EmailVerify.this, R.string.network_error, Toast.LENGTH_SHORT);
                progressDialog.dismiss();
            }
        });
    }

    public boolean validate(final String hex) {
        matcher = pattern.matcher(hex);
        return matcher.matches();
    }

    public static void setContext(Context Context) {
        mContext = Context;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ((Activity) mContext).finish();
        EmailVerify.this.finish();
    }
}


