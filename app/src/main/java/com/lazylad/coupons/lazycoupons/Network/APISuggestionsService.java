package com.lazylad.coupons.lazycoupons.Network;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by Amud on 25/01/16.
 */
public interface APISuggestionsService {

    @GET("/newserver/lazyad/customer/get_coupons")
    void getAllCouponAPICall(@Query("user_code") String user_code,
                             @Query("city_code") int  city_code,
                             Callback<APIResonseModel.GetAllCouponResponseModel> cb);

    @GET("/newserver/lazyad/customer/get_cities")
    void getCitiesAPICall(
            @Query("user_code") String user_code,Callback<APIResonseModel.GetCityResponseModel> cb);

   /* @POST("/newserver/lazyad/customer/user_signup")
    void signUpAPICall(@Body APIRequestModel.SingUpRequestModel param,
                       Callback<APIResonseModel.SignUpResponseModel> cb);
*/
    @GET("/newserver/lazyad/customer/get_coupon_code_details")
    void loadCouponMyDetailAPICall(@Query("coupon_code") String coupon_code,
                                   Callback<APIResonseModel.LoadCouponMyDetailResponseModel> cb);

    @GET("/newserver/lazyad/customer/get_coupon_details")
    void loadCouponAllDetailAPICall(@Query("coupon_id") String coupon_id,
                                    Callback<APIResonseModel.LoadCouponAllDetailResponseModel> cb);

    @POST("/newserver/lazyad/customer/buy_coupon")
    void buyCouponDetailAPICall(@Body APIRequestModel.BuyCouponDetailRequestModel param,
                                Callback<APIResonseModel.BuyouponDetailResponseModel> cb);

    @GET("/newserver/lazyad/customer/get_all_coupon_codes")
    void loadMyCouponAPICall(@Query("user_code") String user_code,
                             @Query("area_code") int area_code,
                             @Query("city_code") int city_code,
                             Callback<APIResonseModel.GetMyCouponResponseModel> cb);













    @POST("/task_manager/v1/lazyPointsinProfile")
    void loadlazyPointsAPICall(@Body APIRequestModel.LazyPointsRequestModel param,
                               Callback<APIResonseModel.LazyPointsResponseModel> cb);

    @POST("/task_manager/v1/redeemBoughtCoupon")
    void redeemCouponDetailAPICall(@Body APIRequestModel.RedeemCouponDetailRequestModel param,
                                   Callback<APIResonseModel.RedeemCouponDetailResponseModel> cb);


    @POST("/task_manager/v1/reportProblemBoughtCoupon")
    void reportCouponAPICall(@Body APIRequestModel.ReportCouponCouponDetailRequestModel param,
                             Callback<APIResonseModel.ReportCouponResponseModel> cb);

    @FormUrlEncoded
    @POST("/newserver/account/walletCashbackByCoupon")
    void postCouponCodeAPICall(@Field("user_code") String user_code, @Field("coupon_code") String coupon_code,
                               Callback<APIResonseModel.CouponPostResponseModel> cb);

    @POST("/newserver/account/getWalletDetails")
    void getWalletDetailsAPICall(@Body APIRequestModel.GetWalletDetailsRequestModel param,
                                 Callback<APIResonseModel.GetWalletDetailsResponseModel> cb);

    @FormUrlEncoded
    @POST("/newserver/account/getAllTransactions")
    void getAllTransactionsAPICall(@Field("user_code") String user_code, @Field("owner_type") int owner_type,
                                   Callback<APIResonseModel.GetAllTransactionsResponseModel> cb);

    @POST("/task_manager/v1/addNumberWithUser")
    void sendNumberToserverAPICall(@Body APIRequestModel.SendNumberToserverRequestModel param,
                                   Callback<APIResonseModel.SendNumberToserverResponseModel> cb);

    @POST("/task_manager/v1/getUserProfileDetails")
    void getUserProfileAPICall(@Body APIRequestModel.GetUserProfileRequestModel param,
                               Callback<APIResonseModel.GetUserProfileResponseModel> cb);

    @POST("/task_manager/v1/userProfileDetails")
    void loadUserProfileAPICall(@Body APIRequestModel.LoudUserProfileRequestModel param,
                                Callback<APIResonseModel.LoadUserProfileResponseModel> cb);

    @POST("/newserver/lazyad/customer/user_login")
    void signInAPICall(@Body APIRequestModel.LogInRequestModel param,
                                Callback<APIResonseModel.SignInResponseModel> cb);



}
