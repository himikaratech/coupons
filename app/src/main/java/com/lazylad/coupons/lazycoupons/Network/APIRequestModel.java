package com.lazylad.coupons.lazycoupons.Network;

import com.google.gson.annotations.SerializedName;
import com.lazylad.coupons.lazycoupons.AccountTransactionsDetailSnippet;
import com.lazylad.coupons.lazycoupons.Snippets.CouponsClicksListSnippet;
import com.lazylad.coupons.lazycoupons.Snippets.CouponsViewsListSnippet;

import java.util.ArrayList;

/**
 * Created by Amud on 25/01/16.
 */
public class APIRequestModel {



    public class LazyPointsRequestModel{
        public int user_code;
    }


    public class GetMyCouponRequestModel{
        public int user_code;

    }

    public class GetAllCouponRequestModel{
        public int user_code;
        public  int city_code;
        public int area_code;
    }

    public class LoadCouponAllDetailRequestModel {
        public  int user_code;
        public String coupon_id;
        public  int city_code;
        public int area_code;
    }

    public class LoadCouponMyDetailRequestModel {
        public  int user_code;
        public String bought_id;
        public  int city_code;
        public int area_code;
    }


    public class BuyCouponDetailRequestModel {
        public String user_code;
        public  int city_id;
        public int area_code;
        public String coupon_id;
    }


    public class RedeemCouponDetailRequestModel{
        public int user_code;
        public  int city_code;
        public int area_code;
        public String bought_id;
    }

    public class ReportCouponCouponDetailRequestModel{
        public int user_code;
        public String bought_id;
        public String report_stmt;
    }


    public class CouponPostModel {
        public String coupon_code;
        public String user_code;
    }

    public class GetWalletDetailsRequestModel {
        public String user_code;
        public int owner_type;
    }

    public class GetAllTransactionsRequestModel {

        @SerializedName("user_code")
        public String user_code;
        @SerializedName("owner_type")
        public int owner_type;
        @SuppressWarnings("previous_tid")
        public int previous_tid;
    }

    public class SendNumberToserverRequestModel {
        public String user_code;
        public String phone_number;
        public String referral_code;
    }

    public class GetUserProfileRequestModel {
        public String user_code;
    }

    public class LoudUserProfileRequestModel {
        public String user_code;
        public String user_email;
        public String user_first_name;
        public String user_last_name;
    }

    public class CouponsClicksRequestModel {
        public ArrayList<CouponsClicksListSnippet> log_list;
    }

    public class CouponsViewsRequestModel {
        public ArrayList<CouponsViewsListSnippet> log_list;
    }

    public class LogInRequestModel {
        public  String phone_number;
    }

    public class SingUpRequestModel {
        public  String username;
        public  String password;
        public  String phone_number;
    }
}
