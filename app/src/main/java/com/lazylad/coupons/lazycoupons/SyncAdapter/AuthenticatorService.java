package com.lazylad.coupons.lazycoupons.SyncAdapter;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * Created by Amud on 29/01/16.
 */

        import android.app.Service;
        import android.content.Intent;
        import android.os.IBinder;

public class AuthenticatorService extends Service {
    private Authenticator mAuthenticator;

    public void onCreate() {
        mAuthenticator = new Authenticator(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mAuthenticator.getIBinder();
    }
}
