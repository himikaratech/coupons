package com.lazylad.coupons.lazycoupons;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lazylad.coupons.lazycoupons.Network.APIRequestModel;
import com.lazylad.coupons.lazycoupons.Network.APIResonseModel;
import com.lazylad.coupons.lazycoupons.Network.APISuggestionsService;

import java.util.ArrayList;

import retrofit.RestAdapter;
import retrofit.RetrofitError;

/**
 * Created by Amud on 25/01/16.
 */

public class CouponAllFragment extends Fragment {
    private String m_userCode;
    RecyclerView recyclerView;
    private ArrayList<CouponAllSnippet> m_CouponAllList;
    private CouponAllAdapter couponAllAdapter;
    private SharedPreferences prefs_location;
    private int areaCode;
    private int cityCode;
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        m_userCode = getArguments().getString("userCode");
        prefs_location = getActivity().getSharedPreferences(
                "com.lazylad.coupons.lazycoupons", Context.MODE_PRIVATE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.coupon_all_fragment_layout, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.coupon_all_list_view);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        loadCoupon(m_userCode);

        return rootView;
    }

    private void loadCoupon(String m_userCode)
    {

        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://52.74.141.166").setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);

        APIRequestModel requestModel=new APIRequestModel();
         cityCode = prefs_location.getInt(EditorConstants.CITY_CODE_CONSTANT, EditorConstants.DEFAULT_CITY_CODE_CONSTANT);
        Log.d("m_userCode",m_userCode);




        apiSuggestionsService.getAllCouponAPICall(m_userCode,cityCode, new retrofit.Callback<APIResonseModel.GetAllCouponResponseModel>() {

            @Override
            public void success(APIResonseModel.GetAllCouponResponseModel getAllCouponResponseModel, retrofit.client.Response response) {


                m_CouponAllList = getAllCouponResponseModel.coupons;
                if(m_CouponAllList != null && !m_CouponAllList.isEmpty()) {
                    couponAllAdapter = new CouponAllAdapter(getActivity(), m_CouponAllList);
                    recyclerView.setAdapter(couponAllAdapter);

                    couponAllAdapter.SetOnItemClickListener(new CouponAllAdapter.OnItemClickListener() {

                        @Override
                        public void onItemClick(View v, int position) {
                            String coupon_id = m_CouponAllList.get(position).id;

                            int clicks_count = 0;
                            String[] clicksSelectionValues = new String[]{LazyCouponContract.CouponsClicksDetails.COLUMN_CLICKS_COUNT};
                            String clicksWhereClause = LazyCouponContract.CouponsStatistics.COLUMN_COUPON_ID + " = ? and " +
                                    LazyCouponContract.CouponsStatistics.COLUMN_COUPON_UP_SYNC + " = ? ";
                            String[] clicksSelectionArgs = new String[]{coupon_id, String.valueOf(0)};


                            Cursor m_cursor = getActivity().getContentResolver().query(
                                    LazyCouponContract.CouponsClicksDetails.CONTENT_URI,
                                    clicksSelectionValues,
                                    clicksWhereClause,
                                    clicksSelectionArgs,
                                    null);

                            if (m_cursor != null && m_cursor.moveToFirst()) {
                                clicks_count = m_cursor.getInt(m_cursor.getColumnIndex(LazyCouponContract.CouponsClicksDetails.COLUMN_CLICKS_COUNT));
                            }
                            clicks_count = clicks_count + 1;
                            ContentValues clicksValues = new ContentValues();
                            clicksValues.put(LazyCouponContract.CouponsClicksDetails.COLUMN_CLICKS_COUNT, clicks_count);
                            int clicks_update = getActivity().getContentResolver().update(LazyCouponContract.CouponsClicksDetails.CONTENT_URI, clicksValues, clicksWhereClause, clicksSelectionArgs);
                            if (clicks_update < 1) {
                                clicksValues.put(LazyCouponContract.CouponsStatistics.COLUMN_COUPON_ID, coupon_id);
                                clicksValues.put(LazyCouponContract.CouponsStatistics.COLUMN_COUPON_UP_SYNC, 0);
                                getActivity().getContentResolver().insert(LazyCouponContract.CouponsClicksDetails.CONTENT_URI, clicksValues);
                            }
                            Intent intent = new Intent(getActivity(), CouponAllDetails.class);

                            intent.putExtra("coupon_id", coupon_id);
                            startActivity(intent);
                        }
                    });
                }

                progressDialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                progressDialog.dismiss();

            }
        });
    }
}
