package com.lazylad.coupons.lazycoupons;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Amud on 30/09/15.
 */
public class NumberClass extends ActionBarActivity {
    TextView phoneNumberEditText;
    ImageView submit;
    Boolean isInternetPresent = false;
    private String mobileNumber;
    private EditText referralTextEdit;
    private TextView skipEditText;
    static Context mContext;
    Bundle bundle;
    SharedPreferences prefs;
    Boolean skippable;
    Boolean skipped;
    int situtation;
    String prefSkippedString;
    String prefVerifiedString;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.number_layout);
        prefs = NumberClass.this.getSharedPreferences(
                "com.lazylad.coupons.lazycoupons", Context.MODE_PRIVATE);
        bundle = new Bundle();
        bundle = getIntent().getExtras();
        skippable = getIntent().getBooleanExtra(NumberParameters.SKIPPABLE_CONSTANT, false);
        situtation = getIntent().getIntExtra(NumberParameters.SITUATION_CONTANT, 0);
        prefSkippedString = getIntent().getStringExtra(NumberParameters.PREF_STRING_CONTANT);
        prefVerifiedString = getIntent().getStringExtra(NumberParameters.PREF_VERIFIED_CONSTANT);

        phoneNumberEditText = (EditText) findViewById(R.id.phoneNrTextEdit);
        submit = (ImageView) findViewById(R.id.submit);
        referralTextEdit = (EditText) findViewById(R.id.referralTextEdit);
        skipEditText = (TextView) findViewById(R.id.skipTextView);
        skipEditText.setVisibility(View.GONE);
        referralTextEdit.setVisibility(View.GONE);
        skipped = false;

        submit.setEnabled(false);

        if (skippable == true)
            skipEditText.setVisibility(View.VISIBLE);
        if (situtation == 1) {
            referralTextEdit.setVisibility(View.VISIBLE);
        }

        final TextWatcher mTextEditorWatcher = new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int value = s.length();
                submit.setEnabled(false);
                submit.setImageDrawable(getResources().getDrawable(R.drawable.blanktick));
                if (value == 10) {

                    isInternetPresent = isNetworkAvailable();
                    if (isInternetPresent) {
                        submit.setImageDrawable(getResources().getDrawable(R.mipmap.done_state));
                        submit.setEnabled(true);
                        mobileNumber = s.toString();
                    } else {
                        Toast.makeText(NumberClass.this, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            public void afterTextChanged(Editable s) {
            }
        };

        phoneNumberEditText.addTextChangedListener(mTextEditorWatcher);

        skipEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = prefs.edit();
                editor.putBoolean(prefSkippedString, true);
                editor.putBoolean(prefVerifiedString, true);
                editor.commit();
                skipped = true;
                onBackPressed();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String referCode = referralTextEdit.getText().toString();
                if ((!referCode.isEmpty()) && (!(referCode == null)) && situtation == 1) {
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putInt(EditorConstants.REFERRAL_JUST_ADDED_CONSTANT, 1);
                    editor.putString(EditorConstants.REFERRAL_CODE_CONSTANT, referCode);
                    editor.commit();
                }

                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt(EditorConstants.NUMBER_JUST_ADDED_CONSTANT, 1);
                editor.putString(EditorConstants.NUMBER_ADDED_CONSTANT, mobileNumber);
                editor.commit();

                Intent intent = new Intent(mContext, NumberVerify.class);
                NumberVerify.setContext(mContext);
                intent.putExtra(NumberParameters.PREF_VERIFIED_CONSTANT, prefVerifiedString);
                intent.putExtra(NumberParameters.SKIPPABLE_CONSTANT, skippable);
                intent.putExtras(bundle);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mContext.startActivity(intent);
                NumberClass.this.finish();
            }
        });

    }

    public static Context getContext() {
        return mContext;
    }

    public static void setContext(Context Context) {
        mContext = Context;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (skippable == false || skipped == false)
            ((Activity) mContext).finish();
    }
}
