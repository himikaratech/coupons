package com.lazylad.coupons.lazycoupons;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Point;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.lazylad.coupons.lazycoupons.Snippets.CouponsViewsListSnippet;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Amud on 25/01/16.
 */


public class CouponAllAdapter extends RecyclerView.Adapter<CouponAllAdapter.ViewHolder> {

    private ArrayList<CouponAllSnippet> m_CouponAllList;
    private Activity m_activity;
    static OnItemClickListener mItemClickListener;
    ArrayList<String> imageList = new ArrayList<String>();

    public CouponAllAdapter(Context context, ArrayList<CouponAllSnippet> couponAllList) {
        m_activity = (Activity) context;
        this.m_CouponAllList = couponAllList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.coupon_all_list_layout, parent, false);
        return new ViewHolder(v, viewType);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        if (!m_CouponAllList.isEmpty()) {
            String coupon_id = m_CouponAllList.get(position).id;
            String couponShortDesc = m_CouponAllList.get(position).short_desc;
            String image_url =  m_CouponAllList.get(position).img_address;
            boolean image_flag =  m_CouponAllList.get(position).img_flag;
            WindowManager wm = (WindowManager) m_activity.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;


            if (image_flag && image_url != null && !image_url.isEmpty()) {
                Picasso.with(m_activity)
                        .load(image_url)
                        .resize(width, 0)
                        .placeholder(R.drawable.loading_big)
                        .error(R.drawable.loading_big)
                        .into(holder.m_couponImage);
            }

            int views_count = 0;
            String[] viewsSelectionValues = new String[]{LazyCouponContract.CouponsViewsDetails.COLUMN_VIEWS_COUNT};
            String viewsWhereClause = LazyCouponContract.CouponsStatistics.COLUMN_COUPON_ID + " = ? and " +
                    LazyCouponContract.CouponsStatistics.COLUMN_COUPON_UP_SYNC + " = ? ";
            String[] viewsSelectionArgs = new String[]{coupon_id, String.valueOf(0)};


            Cursor m_cursor = m_activity.getContentResolver().query(
                    LazyCouponContract.CouponsViewsDetails.CONTENT_URI,
                    viewsSelectionValues,
                    viewsWhereClause,
                    viewsSelectionArgs,
                    null);

            if (m_cursor != null && m_cursor.moveToFirst()) {
                views_count = m_cursor.getInt(m_cursor.getColumnIndex(LazyCouponContract.CouponsViewsDetails.COLUMN_VIEWS_COUNT));
            }
            views_count = views_count + 1;
            ContentValues viewsValues = new ContentValues();
            viewsValues.put(LazyCouponContract.CouponsViewsDetails.COLUMN_VIEWS_COUNT, views_count);
            int views_update = m_activity.getContentResolver().update(LazyCouponContract.CouponsViewsDetails.CONTENT_URI, viewsValues, viewsWhereClause, viewsSelectionArgs);
            if (views_update < 1) {
                viewsValues.put(LazyCouponContract.CouponsStatistics.COLUMN_COUPON_ID, coupon_id);
                viewsValues.put(LazyCouponContract.CouponsStatistics.COLUMN_COUPON_UP_SYNC, 0);
                m_activity.getContentResolver().insert(LazyCouponContract.CouponsViewsDetails.CONTENT_URI, viewsValues);
            }

            holder.m_couponNameTextView.setText( m_CouponAllList.get(position).name);
            holder.m_couponShortDescTextView.setText(couponShortDesc);
        }
    }


    @Override
    public int getItemCount() {
        return m_CouponAllList.size();
    }

    public void onItemClick(View view, int position) {
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView m_couponNameTextView;
        TextView m_couponShortDescTextView;
        ImageView m_couponImage;

        public ViewHolder(View itemView, int flag) {
            super(itemView);

            m_couponNameTextView = (TextView) itemView.findViewById(R.id.coupon_name);
            m_couponShortDescTextView = (TextView) itemView.findViewById(R.id.coupon_short_desc);
            m_couponImage = (ImageView) itemView.findViewById(R.id.coupon_image);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getPosition());
            }
        }
    }

    public Object getItem(int position) {
        if (m_CouponAllList != null) {
            return m_CouponAllList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_CouponAllList != null) {
            return position;
        }
        return 0;
    }
}