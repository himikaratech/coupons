package com.lazylad.coupons.lazycoupons.SyncAdapter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SyncRequest;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;


import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;
import com.lazylad.coupons.lazycoupons.EditorConstants;
import com.lazylad.coupons.lazycoupons.LazyCouponContract;
import com.lazylad.coupons.lazycoupons.Network.APIRequestModel;
import com.lazylad.coupons.lazycoupons.R;
import com.lazylad.coupons.lazycoupons.Snippets.CouponsClicksListSnippet;
import com.lazylad.coupons.lazycoupons.LazyCouponContract.CouponsClicksDetails;
import com.lazylad.coupons.lazycoupons.LazyCouponContract.CouponsStatistics;
import com.lazylad.coupons.lazycoupons.Network.APIResonseModel;
import com.lazylad.coupons.lazycoupons.Network.APISuggestionsService;




import org.apache.http.HttpResponse;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Amud on 29/01/16.
 */

public class CouponClicksSycnAdapter extends AbstractThreadedSyncAdapter {
    public final String LOG_TAG = CouponClicksSycnAdapter.class.getSimpleName();
    public static final int SYNC_INTERVAL = 60 * 60;
    public static final int SYNC_FLEXTIME = SYNC_INTERVAL / 2;

    public CouponClicksSycnAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        // TODO Auto-generated constructor stub
    }

    @SuppressLint("NewApi")
    public CouponClicksSycnAdapter(
            Context context,
            boolean autoInitialize,
            boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority,
                              ContentProviderClient provider, SyncResult syncResult) {
        //TODO connect to the server
        //TODO To write code to download and upload data from server to local content provider
        //TODO Handle data conflicts
        //TODO cleanup

        Log.e("syncing1", "clickssstarted");

        String user_code;
        SharedPreferences m_sharedPref = getContext().getSharedPreferences(
                "com.lazylad.coupons.lazycoupons.SyncAdapter", Context.MODE_PRIVATE);
        user_code = m_sharedPref.getString(EditorConstants.USER_CODE_CONSTANT, EditorConstants.DEFAULT_USER_CODE_CONSTANT);


        ArrayList<CouponsClicksListSnippet> couponsClickList = new ArrayList<CouponsClicksListSnippet>();
        String clicksWhereClause = CouponsStatistics.COLUMN_COUPON_UP_SYNC + " = ? ";
        String[] clicksSelectionArgs = new String[]{String.valueOf(0)};


        Cursor m_cursor = getContext().getContentResolver().query(
                CouponsClicksDetails.CONTENT_URI,
                null,
                clicksWhereClause,
                clicksSelectionArgs,
                null);


        if (m_cursor != null && m_cursor.moveToFirst()) {

            if (m_cursor.getCount() != 0) {
                for (int j = 0; j < m_cursor.getCount(); j++) {
                    Log.e("logging", "here");


                    String coupon_id = m_cursor.getString(m_cursor.getColumnIndex(CouponsStatistics.COLUMN_COUPON_ID));
                    int click_count = m_cursor.getInt(m_cursor.getColumnIndex(CouponsClicksDetails.COLUMN_CLICKS_COUNT));

                    CouponsClicksListSnippet couponsClicksListSnippet = new CouponsClicksListSnippet(coupon_id, click_count,user_code);
                    couponsClickList.add(couponsClicksListSnippet);
                    m_cursor.moveToNext();

                }
                markClicksSynced(clicksWhereClause,clicksSelectionArgs);


            }
        }


        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.CouponsClicksRequestModel couponsClicksRequestModel = requestModel.new CouponsClicksRequestModel();
        couponsClicksRequestModel.log_list = couponsClickList;

        Gson gson = new Gson();
        String json = gson.toJson(couponsClicksRequestModel);


        try {

            final String ST_CODE = "st_code";


            HttpPost post = new HttpPost();
            post.setHeader("Content-Type", "application/json; charset=utf-8");
            post.setURI(new URI("http://52.74.42.160/newserver/lazypos/send_orders"));

            Log.d("jsonClicks", json);


            post.setEntity(new StringEntity(json));

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpResponse httpResponse = httpClient.execute(post);
            if (httpResponse.getStatusLine().getStatusCode() == 200) {
                String responseStr = EntityUtils.toString(httpResponse.getEntity());
                Log.d("responseStrClicks", responseStr);
                APIResonseModel.CouponsClicksResponseModel couponsClicksResponseModel = gson.fromJson(responseStr, APIResonseModel.CouponsClicksResponseModel.class);
                boolean success = couponsClicksResponseModel.success;
                if (success == true) {
                    m_cursor.moveToFirst();
                    if (m_cursor != null && m_cursor.moveToFirst()) {
                        if (m_cursor.getCount() != 0) {
                            for (int i = 0; i < m_cursor.getCount(); i++) {
                                Log.d("_id", i + "");
                                int upSyncID = m_cursor.getInt(m_cursor.getColumnIndex(CouponsClicksDetails._ID));
                                String whereClause = CouponsClicksDetails._ID + " = ? ";
                                String[] selectionArgs = new String[]{String.valueOf(upSyncID)};
                                getContext().getContentResolver().delete(CouponsClicksDetails.CONTENT_URI,whereClause,selectionArgs);
                                m_cursor.moveToNext();
                            }
                        }
                    }



                } else {
                    makeClicksUpsyncToUnSync(m_cursor);
                }
            } else {
                makeClicksUpsyncToUnSync(m_cursor);

            }


            if (m_cursor != null) m_cursor.close();

        } catch (IOException e) {
            Log.e(LOG_TAG, "Error ", e);
            // If the code didn't successfully get the weather data, there's no point in attempting
            // to parse it.
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return;
    }


    private void makeClicksUpsyncToUnSync(Cursor m_cursor) {
        m_cursor.moveToFirst();
        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    Log.d("_id", i + "");
                    int upSyncID = m_cursor.getInt(m_cursor.getColumnIndex(CouponsClicksDetails._ID));
                    String whereClause = CouponsClicksDetails._ID + " = ? ";
                    String[] selectionArgs = new String[]{String.valueOf(upSyncID)};
                    ContentValues values = new ContentValues();
                    values.put(CouponsStatistics.COLUMN_COUPON_UP_SYNC, 0);
                    getContext().getContentResolver().update(CouponsClicksDetails.CONTENT_URI, values, whereClause, selectionArgs);
                    m_cursor.moveToNext();
                }
            }
        }
    }

    private void markClicksSynced(String clicksWhereClause, String[] clicksSelectionArgs) {
        ContentValues updateUpSyncValues = new ContentValues();
        updateUpSyncValues.put(LazyCouponContract.CouponsStatistics.COLUMN_COUPON_UP_SYNC, 1);
        getContext().getContentResolver().update(LazyCouponContract.CouponsClicksDetails.CONTENT_URI, updateUpSyncValues, clicksWhereClause, clicksSelectionArgs);
    }









    public static void configurePeriodicSync(Context context, int syncInterval, int flexTime) {
        Account account = getSyncAccount(context);
        String authority = LazyCouponContract.CONTENT_AUTHORITY;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            SyncRequest request = new SyncRequest.Builder().
                    syncPeriodic(syncInterval, flexTime).
                    setSyncAdapter(account, authority).
                    setExtras(new Bundle()).build();
            ContentResolver.requestSync(request);
        } else {
            ContentResolver.addPeriodicSync(account,
                    authority, new Bundle(), syncInterval);
        }
    }


    public static void syncImmediately(Context context) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        ContentResolver.requestSync(getSyncAccount(context),
                LazyCouponContract.CONTENT_AUTHORITY, bundle);

        Log.d("clicks", "syncing");
    }


    public static Account getSyncAccount(Context context) {
        AccountManager accountManager =
                (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);

        Account newAccount = new Account(
                context.getString(R.string.app_name), context.getString(R.string.sync_account_type));

        if (null == accountManager.getPassword(newAccount)) {

        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
            if (!accountManager.addAccountExplicitly(newAccount, "", null)) {
                return null;
            }
            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call ContentResolver.setIsSyncable(account, AUTHORITY, 1)
             * here.
             */
            Log.d("account", "created");
            onAccountCreated(newAccount, context);
        } else {
            Log.d("account", "not created");
        }

        return newAccount;
    }

    private static void onAccountCreated(Account newAccount, Context context) {


        /*
         * Since we've created an account
         */
        CouponClicksSycnAdapter.configurePeriodicSync(context, SYNC_INTERVAL, SYNC_FLEXTIME);

        /*
         * Without calling setSyncAutomatically, our periodic sync will not be enabled.
         */
        ContentResolver.setSyncAutomatically(newAccount,LazyCouponContract.CONTENT_AUTHORITY, true);

        /*
         * Finally, let's do a sync to get things started
         */
        //  syncImmediately(context);
    }

    public static void initializeSyncAdapter(Context context) {

        getSyncAccount(context);
    }

}


