package com.lazylad.coupons.lazycoupons;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lazylad.coupons.lazycoupons.Network.APIRequestModel;
import com.lazylad.coupons.lazycoupons.Network.APIResonseModel;
import com.lazylad.coupons.lazycoupons.Network.APISuggestionsService;
import com.squareup.picasso.Picasso;

import retrofit.RestAdapter;
import retrofit.RetrofitError;

/**
 * Created by Amud on 25/01/16.
 */

public class CouponMyDetails extends ActionBarActivity {
    private Toolbar m_toolbar;
    private String m_couponCode;
    private int m_userCode;
    private boolean buyCouponFlag;
    private SharedPreferences prefs_location;
    private int areaCode;
    private int cityCode;
    private Button m_submitButton;
    private ImageView m_couponImageView;
    private TextView m_couponNameTV;
    private TextView m_shortDescTV;
    private TextView m_descTV;
    private TextView m_couponCodeTV;
    private TextView m_termsAndConditions;

    private CouponMyDetailSnippet couponMyDetailSnippet;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        setContentView(R.layout.coupon_my_details_layout);
       /* m_toolbar = (Toolbar) findViewById(R.id.coupon_toolbar);
        m_toolbar.setPadding(0, getStatusBarHeight(), 0, 0);
        if (m_toolbar != null)
            m_toolbar.inflateMenu(R.menu.coupon_toolbar);
        setSupportActionBar(m_toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
*/
        /*
        areaCode = Integer.parseInt(prefs_location.getString(EditorConstants.AREA_SELECTED_CONSTANT, "0"));
        cityCode = Integer.parseInt(prefs_location.getString(EditorConstants.CITY_SELECTED_CONSTANT, "0"));

      */
        SharedPreferences m_sharedPref = getSharedPreferences(
                "com.lazylad.coupons.lazycoupons", Context.MODE_PRIVATE);

        cityCode = m_sharedPref.getInt(EditorConstants.CITY_CODE_CONSTANT, EditorConstants.DEFAULT_CITY_CODE_CONSTANT);
        areaCode = 1;
        m_couponCode = getIntent().getStringExtra("coupon_code");


        m_couponImageView = (ImageView) findViewById(R.id.coupon_image);
        m_couponNameTV = (TextView) findViewById(R.id.coupon_name);
        m_shortDescTV = (TextView) findViewById(R.id.short_desc);
        m_descTV = (TextView) findViewById(R.id.desc);
        m_couponCodeTV = (TextView) findViewById(R.id.coupon_code);
        m_termsAndConditions= (TextView) findViewById(R.id.terms_and_conditions);



        m_couponNameTV.setVisibility(View.GONE);
        m_shortDescTV.setVisibility(View.GONE);
        m_descTV.setVisibility(View.GONE);
        m_couponCodeTV.setVisibility(View.GONE);
        m_termsAndConditions.setVisibility(View.GONE);




        m_couponImageView.setVisibility(View.GONE);
    //    m_submitButton.setVisibility(View.GONE);

        buyCouponFlag = false;
        couponMyDetailSnippet = new CouponMyDetailSnippet();

        Cursor m_cursor = getContentResolver().query(
                LazyCouponContract.UserDetailsEntry.CONTENT_URI,
                null,
                null,
                null,
                null);

        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() == 1) {
                m_userCode = m_cursor.getInt(1);
            }
        }

      /*  m_submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redeemCoupon(m_couponCode);


            }
        });
*/

        loadCouponDetail(m_couponCode);
    }

    private void loadCouponDetail(String couponCode) {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://52.74.141.166").setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);


        apiSuggestionsService.loadCouponMyDetailAPICall(couponCode, new retrofit.Callback<APIResonseModel.LoadCouponMyDetailResponseModel>() {

            @Override
            public void success(APIResonseModel.LoadCouponMyDetailResponseModel loadCouponMyDetailResponseModel, retrofit.client.Response response) {

                boolean success = loadCouponMyDetailResponseModel.success;
                if (success ) {
                    couponMyDetailSnippet = loadCouponMyDetailResponseModel.coupon_code_details;

                    if(couponMyDetailSnippet != null ) {

                        String coupon_name = couponMyDetailSnippet.coupon.name;
                        String coupon_code = couponMyDetailSnippet.coupon_code;
                        String short_desc = couponMyDetailSnippet.coupon.short_desc;
                        String desc = couponMyDetailSnippet.coupon.desc;
                        String terms = couponMyDetailSnippet.coupon.terms;



                        m_couponNameTV.setVisibility(View.VISIBLE);
                        m_shortDescTV.setVisibility(View.VISIBLE);
                        m_descTV.setVisibility(View.VISIBLE);
                        m_couponCodeTV.setVisibility(View.VISIBLE);
                        m_termsAndConditions.setVisibility(View.VISIBLE);


                        boolean image_flag = couponMyDetailSnippet.coupon.img_flag;
                        if (image_flag) {

                            Display display = getWindowManager().getDefaultDisplay();
                            Point size = new Point();
                            display.getSize(size);
                            int width = size.x;
                            String image_address = couponMyDetailSnippet.coupon.img_address;
                            {
                                if (image_address == null || image_address == "")
                                    image_address = "\"\"";
                                Picasso.with(CouponMyDetails.this)
                                        .load(image_address)
                                        .resize(width, 0)
                                        .placeholder(R.drawable.loading_big)
                                        .error(R.drawable.loading_big)
                                        .into(m_couponImageView);

                                m_couponImageView.setVisibility(View.VISIBLE);
                            }
                        }

                        Log.e("coupon_name", coupon_code);
                        m_couponNameTV.setText(coupon_name);
                        m_couponCodeTV.setText(coupon_code);
                        m_shortDescTV.setText(short_desc);
                        m_descTV.setText(desc);
                        m_termsAndConditions.setText(terms);

                      //  m_toolbar.setVisibility(View.VISIBLE);
                      //  m_submitButton.setVisibility(View.VISIBLE);

                    }
                    progressDialog.dismiss();
                } else
                    Log.d("ser_ty_err", "service_type_from_server_error");
            }


            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                progressDialog.dismiss();
            }
        });
    }


    private void redeemCoupon(String couponIdentifier) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://www.angulartechnologies.com").setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);

        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.RedeemCouponDetailRequestModel redeemCouponDetailRequestModel = requestModel.new RedeemCouponDetailRequestModel();
        redeemCouponDetailRequestModel.bought_id = couponIdentifier;
        redeemCouponDetailRequestModel.user_code = m_userCode;
        redeemCouponDetailRequestModel.area_code = areaCode;
        redeemCouponDetailRequestModel.city_code = cityCode;
        apiSuggestionsService.redeemCouponDetailAPICall(redeemCouponDetailRequestModel, new retrofit.Callback<APIResonseModel.RedeemCouponDetailResponseModel>() {

            @Override
            public void success(APIResonseModel.RedeemCouponDetailResponseModel redeemCouponDetailResponseModel, retrofit.client.Response response) {

                int success = redeemCouponDetailResponseModel.error;
                if (success == 1) {
                    Toast.makeText(CouponMyDetails.this, "Coupon has been redeemed, Thanks", Toast.LENGTH_SHORT).show();
                    m_submitButton.setEnabled(false);
                    buyCouponFlag = true;
                    progressDialog.dismiss();
                    onBackPressed();
                } else
                    Log.d("ser_ty_err", "service_type_from_server_error");
            }


            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                progressDialog.dismiss();
            }
        });
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (buyCouponFlag) {
            Intent intent = new Intent(this, CouponClass.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("buyCouponFlag", buyCouponFlag);
            this.startActivity(intent);
            this.finish();
        }
    }
}
