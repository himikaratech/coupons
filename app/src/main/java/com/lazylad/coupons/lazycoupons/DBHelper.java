package com.lazylad.coupons.lazycoupons;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Amud on 14/09/15.
 */
public class DBHelper extends SQLiteOpenHelper {
    private static int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "LazyLadCouponData";


    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        final String SQL_CREATE_USER_DETAILS =
                "CREATE TABLE IF NOT EXISTS " + LazyCouponContract.UserDetailsEntry.TABLE_NAME +
                        "(" +
                        LazyCouponContract.UserDetailsEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        LazyCouponContract.UserDetailsEntry.COLUMN_USER_CODE + " INTEGER, " +
                        LazyCouponContract.UserDetailsEntry.COLUMN_VERIFIED_FLAG + " INT, " +
                        LazyCouponContract.UserDetailsEntry.COLUMN_VERIFIED_NUMBER + " TEXT, " +
                        LazyCouponContract.UserDetailsEntry.COLUMN_EMAIL_FLAG + " INT, " +
                        LazyCouponContract.UserDetailsEntry.COLUMN_EMAIL_ID + " TEXT " +
                        " );";

        final String SQL_CREATE_COUPONS_VIEWS_DETAILS =
                "CREATE TABLE IF NOT EXISTS " + LazyCouponContract.CouponsViewsDetails.TABLE_NAME +
                        "(" +
                        LazyCouponContract.CouponsViewsDetails._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        LazyCouponContract.CouponsStatistics.COLUMN_COUPON_ID + " TEXT, " +
                        LazyCouponContract.CouponsViewsDetails.COLUMN_VIEWS_COUNT+ " INTEGER DEFAULT 0, " +
                        LazyCouponContract.CouponsStatistics.COLUMN_COUPON_UP_SYNC + " INTEGER DEFAULT 0 , " +
                        LazyCouponContract.CouponsStatistics.COLUMN_TIME_STAMP + " DEFAULT CURRENT_TIMESTAMP " +
                        " );";

        final String SQL_CREATE_COUPONS_CLICKS_DETAILS =
                "CREATE TABLE IF NOT EXISTS " + LazyCouponContract.CouponsClicksDetails.TABLE_NAME +
                        "(" +
                        LazyCouponContract.CouponsClicksDetails._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        LazyCouponContract.CouponsStatistics.COLUMN_COUPON_ID + " TEXT, " +
                        LazyCouponContract.CouponsClicksDetails.COLUMN_CLICKS_COUNT+ " INTEGER DEFAULT 0, " +
                        LazyCouponContract.CouponsStatistics.COLUMN_COUPON_UP_SYNC + " INTEGER DEFAULT 0 , " +
                        LazyCouponContract.CouponsStatistics.COLUMN_TIME_STAMP + " DEFAULT CURRENT_TIMESTAMP " +
                        " );";

        db.execSQL(SQL_CREATE_USER_DETAILS);
        db.execSQL(SQL_CREATE_COUPONS_VIEWS_DETAILS);
        db.execSQL(SQL_CREATE_COUPONS_CLICKS_DETAILS);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + LazyCouponContract.UserDetailsEntry.TABLE_NAME);
        onCreate(db);
    }
}
