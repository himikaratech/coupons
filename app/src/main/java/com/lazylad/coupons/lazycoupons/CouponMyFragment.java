package com.lazylad.coupons.lazycoupons;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lazylad.coupons.lazycoupons.Network.APIRequestModel;
import com.lazylad.coupons.lazycoupons.Network.APIResonseModel;
import com.lazylad.coupons.lazycoupons.Network.APISuggestionsService;

import java.util.ArrayList;

import retrofit.RestAdapter;
import retrofit.RetrofitError;

/**
 * Created by Amud on 25/01/16.
 */

public class CouponMyFragment extends Fragment {
    private String m_userCode;
    RecyclerView recyclerView;
    private ArrayList<CouponMySnippet> m_CouponMyList;
    private CouponMyAdapter couponMyAdapter;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        m_userCode = getArguments().getString("userCode");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.coupon_my_fragment_layout, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.coupon_my_list_view);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(couponMyAdapter);

        loadCoupon(m_userCode);
        return rootView;
    }

    private void loadCoupon(String m_userCode)
    {
        SharedPreferences m_sharedPref = getContext().getSharedPreferences(
                "com.lazylad.coupons.lazycoupons", Context.MODE_PRIVATE);
        int city_code = m_sharedPref.getInt(EditorConstants.CITY_CODE_CONSTANT, EditorConstants.DEFAULT_CITY_CODE_CONSTANT);

        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://52.74.141.166").setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        APISuggestionsService apiSuggestionsService = restAdapter.create(APISuggestionsService.class);



        apiSuggestionsService.loadMyCouponAPICall(m_userCode,1,city_code, new retrofit.Callback<APIResonseModel.GetMyCouponResponseModel>() {

            @Override
            public void success(APIResonseModel.GetMyCouponResponseModel getMyCouponResponseModel, retrofit.client.Response response) {

                boolean success=getMyCouponResponseModel.success;
                Log.d("m_CouponMyListsuccess",success+"");

                if(success) {

                    m_CouponMyList = getMyCouponResponseModel.coupons;
                    if(m_CouponMyList != null && !m_CouponMyList.isEmpty()) {
                        couponMyAdapter = new CouponMyAdapter(getActivity(), m_CouponMyList);
                        recyclerView.setAdapter(couponMyAdapter);
                        couponMyAdapter.SetOnItemClickListener(new CouponMyAdapter.OnItemClickListener() {

                            @Override
                            public void onItemClick(View v, int position) {

                                String coupon_code = m_CouponMyList.get(position).coupon_code;
                                Intent intent = new Intent(getActivity(), CouponMyDetails.class);
                                intent.putExtra("coupon_code", coupon_code);
                                startActivity(intent);
                            }
                        });
                    }
                }
                progressDialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("retro", error + "");
                progressDialog.dismiss();
            }
        });
    }
}

