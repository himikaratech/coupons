package com.lazylad.coupons.lazycoupons.SyncAdapter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SyncRequest;
import android.content.SyncResult;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.lazylad.coupons.lazycoupons.EditorConstants;
import com.lazylad.coupons.lazycoupons.LazyCouponContract;
import com.lazylad.coupons.lazycoupons.Network.APIRequestModel;
import com.lazylad.coupons.lazycoupons.Network.APIResonseModel;
import com.lazylad.coupons.lazycoupons.R;
import com.lazylad.coupons.lazycoupons.Snippets.CouponsClicksListSnippet;
import com.lazylad.coupons.lazycoupons.Snippets.CouponsViewsListSnippet;
import com.lazylad.coupons.lazycoupons.Snippets.CouponsViewsListSnippet;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

/**
 * Created by Amud on 29/01/16.
 */
public class CouponViewsSycnAdapter extends AbstractThreadedSyncAdapter {
    public final String LOG_TAG = CouponViewsSycnAdapter.class.getSimpleName();
    public static final int SYNC_INTERVAL = 60 * 60;
    public static final int SYNC_FLEXTIME = SYNC_INTERVAL / 2;

    public CouponViewsSycnAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        // TODO Auto-generated constructor stub
    }

    @SuppressLint("NewApi")
    public CouponViewsSycnAdapter(
            Context context,
            boolean autoInitialize,
            boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority,
                              ContentProviderClient provider, SyncResult syncResult) {
        //TODO connect to the server
        //TODO To write code to download and upload data from server to local content provider
        //TODO Handle data conflicts
        //TODO cleanup

        Log.e("syncing1", "viewsssstarted");

        String user_code;
        SharedPreferences m_sharedPref = getContext().getSharedPreferences(
                "com.lazylad.coupons.lazycoupons", Context.MODE_PRIVATE);
        user_code = m_sharedPref.getString(EditorConstants.USER_CODE_CONSTANT, EditorConstants.DEFAULT_USER_CODE_CONSTANT);

        Log.d("user_codeSync",user_code);


        ArrayList<CouponsViewsListSnippet> couponsViewsList = new ArrayList<CouponsViewsListSnippet>();
        String viewsWhereClause = LazyCouponContract.CouponsStatistics.COLUMN_COUPON_UP_SYNC + " = ? ";
        String[] viewsSelectionArgs = new String[]{String.valueOf(0)};


        Cursor m_cursor = getContext().getContentResolver().query(
                LazyCouponContract.CouponsViewsDetails.CONTENT_URI,
                null,
                viewsWhereClause,
                viewsSelectionArgs,
                null);


        if (m_cursor != null && m_cursor.moveToFirst()) {

            if (m_cursor.getCount() != 0) {
                for (int j = 0; j < m_cursor.getCount(); j++) {
                    Log.e("logging", "here");


                    String coupon_id = m_cursor.getString(m_cursor.getColumnIndex(LazyCouponContract.CouponsStatistics.COLUMN_COUPON_ID));
                    int views_count = m_cursor.getInt(m_cursor.getColumnIndex(LazyCouponContract.CouponsViewsDetails.COLUMN_VIEWS_COUNT));

                    CouponsViewsListSnippet couponsViewsListSnippet = new CouponsViewsListSnippet(coupon_id, views_count, user_code);
                    couponsViewsList.add(couponsViewsListSnippet);
                    m_cursor.moveToNext();

                }

            }
        }


        APIRequestModel requestModel = new APIRequestModel();
        final APIRequestModel.CouponsViewsRequestModel couponsViewsRequestModel = requestModel.new CouponsViewsRequestModel();
        couponsViewsRequestModel.log_list = couponsViewsList;

        Gson gson = new Gson();
        String json = gson.toJson(couponsViewsRequestModel);


        try {

            HttpPost post = new HttpPost();
            post.setHeader("Content-Type", "application/json; charset=utf-8");
            post.setURI(new URI("http://52.74.141.166/newserver/lazyad/customer/coupons_views"));

            Log.d("jsonViews", json);


            post.setEntity(new StringEntity(json));

            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpResponse httpResponse = httpClient.execute(post);
            if (httpResponse.getStatusLine().getStatusCode() == 200) {
                String responseStr = EntityUtils.toString(httpResponse.getEntity());
                Log.d("responseStrViews", responseStr);
                APIResonseModel.CouponsViewsResponseModel couponsViewsResponseModel = gson.fromJson(responseStr, APIResonseModel.CouponsViewsResponseModel.class);
                boolean success = couponsViewsResponseModel.success;
                if (success == true) {
                    m_cursor.moveToFirst();
                    if (m_cursor != null && m_cursor.moveToFirst()) {
                        if (m_cursor.getCount() != 0) {
                            for (int i = 0; i < m_cursor.getCount(); i++) {
                                Log.d("_id", i + "");
                                int upSyncID = m_cursor.getInt(m_cursor.getColumnIndex(LazyCouponContract.CouponsViewsDetails._ID));
                                String whereClause = LazyCouponContract.CouponsViewsDetails._ID + " = ? ";
                                String[] selectionArgs = new String[]{String.valueOf(upSyncID)};
                                getContext().getContentResolver().delete(LazyCouponContract.CouponsViewsDetails.CONTENT_URI, whereClause, selectionArgs);
                                m_cursor.moveToNext();
                            }
                            markViewsSynced(viewsWhereClause, viewsSelectionArgs);
                        }
                    }


                } else {
                    makeViewsUpsyncToUnSync(m_cursor);
                }
            } else {
                makeViewsUpsyncToUnSync(m_cursor);

            }


            if (m_cursor != null) m_cursor.close();


            ArrayList<CouponsClicksListSnippet> couponsClickList = new ArrayList<CouponsClicksListSnippet>();
            String clicksWhereClause = LazyCouponContract.CouponsStatistics.COLUMN_COUPON_UP_SYNC + " = ? ";
            String[] clicksSelectionArgs = new String[]{String.valueOf(0)};


            m_cursor = getContext().getContentResolver().query(
                    LazyCouponContract.CouponsClicksDetails.CONTENT_URI,
                    null,
                    clicksWhereClause,
                    clicksSelectionArgs,
                    null);


            if (m_cursor != null && m_cursor.moveToFirst()) {

                if (m_cursor.getCount() != 0) {
                    for (int j = 0; j < m_cursor.getCount(); j++) {
                        Log.e("logging", "here");


                        String coupon_id = m_cursor.getString(m_cursor.getColumnIndex(LazyCouponContract.CouponsStatistics.COLUMN_COUPON_ID));
                        int click_count = m_cursor.getInt(m_cursor.getColumnIndex(LazyCouponContract.CouponsClicksDetails.COLUMN_CLICKS_COUNT));

                        CouponsClicksListSnippet couponsClicksListSnippet = new CouponsClicksListSnippet(coupon_id, click_count, user_code);
                        couponsClickList.add(couponsClicksListSnippet);
                        m_cursor.moveToNext();

                    }
                    markClicksSynced(clicksWhereClause, clicksSelectionArgs);

                }
            }


            requestModel = new APIRequestModel();
            final APIRequestModel.CouponsClicksRequestModel couponsClicksRequestModel = requestModel.new CouponsClicksRequestModel();
            couponsClicksRequestModel.log_list = couponsClickList;

            gson = new Gson();
            json = gson.toJson(couponsClicksRequestModel);


            post = new HttpPost();
            post.setHeader("Content-Type", "application/json; charset=utf-8");
            post.setURI(new URI("http://52.74.141.166/newserver/lazyad/customer/coupons_clicks"));

            Log.d("jsonClicks", json);


            post.setEntity(new StringEntity(json));

            httpClient = new DefaultHttpClient();
            httpResponse = httpClient.execute(post);
            if (httpResponse.getStatusLine().getStatusCode() == 200) {
                String responseStr = EntityUtils.toString(httpResponse.getEntity());
                Log.d("responseStrClicks", responseStr);
                APIResonseModel.CouponsClicksResponseModel couponsClicksResponseModel = gson.fromJson(responseStr, APIResonseModel.CouponsClicksResponseModel.class);
                boolean success = couponsClicksResponseModel.success;
                if (success == true) {
                    m_cursor.moveToFirst();
                    if (m_cursor != null && m_cursor.moveToFirst()) {
                        if (m_cursor.getCount() != 0) {
                            for (int i = 0; i < m_cursor.getCount(); i++) {
                                Log.d("_id", i + "");
                                int upSyncID = m_cursor.getInt(m_cursor.getColumnIndex(LazyCouponContract.CouponsClicksDetails._ID));
                                String whereClause = LazyCouponContract.CouponsClicksDetails._ID + " = ? ";
                                String[] selectionArgs = new String[]{String.valueOf(upSyncID)};
                                getContext().getContentResolver().delete(LazyCouponContract.CouponsClicksDetails.CONTENT_URI, whereClause, selectionArgs);
                                m_cursor.moveToNext();
                            }
                        }
                    }


                } else {
                    makeClicksUpsyncToUnSync(m_cursor);
                }
            } else {
                makeClicksUpsyncToUnSync(m_cursor);

            }


            if (m_cursor != null) m_cursor.close();


        } catch (IOException e) {
            Log.e(LOG_TAG, "Error ", e);
            // If the code didn't successfully get the weather data, there's no point in attempting
            // to parse it.
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return;
    }

    private void makeClicksUpsyncToUnSync(Cursor m_cursor) {
        m_cursor.moveToFirst();
        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    Log.d("_id", i + "");
                    int upSyncID = m_cursor.getInt(m_cursor.getColumnIndex(LazyCouponContract.CouponsClicksDetails._ID));
                    String whereClause = LazyCouponContract.CouponsClicksDetails._ID + " = ? ";
                    String[] selectionArgs = new String[]{String.valueOf(upSyncID)};
                    ContentValues values = new ContentValues();
                    values.put(LazyCouponContract.CouponsStatistics.COLUMN_COUPON_UP_SYNC, 0);
                    getContext().getContentResolver().update(LazyCouponContract.CouponsClicksDetails.CONTENT_URI, values, whereClause, selectionArgs);
                    m_cursor.moveToNext();
                }
            }
        }
    }


    private void makeViewsUpsyncToUnSync(Cursor m_cursor) {
        m_cursor.moveToFirst();
        if (m_cursor != null && m_cursor.moveToFirst()) {
            if (m_cursor.getCount() != 0) {
                for (int i = 0; i < m_cursor.getCount(); i++) {
                    Log.d("_id", i + "");
                    int upSyncID = m_cursor.getInt(m_cursor.getColumnIndex(LazyCouponContract.CouponsViewsDetails._ID));
                    String whereClause = LazyCouponContract.CouponsViewsDetails._ID + " = ? ";
                    String[] selectionArgs = new String[]{String.valueOf(upSyncID)};
                    ContentValues values = new ContentValues();
                    values.put(LazyCouponContract.CouponsStatistics.COLUMN_COUPON_UP_SYNC, 0);
                    getContext().getContentResolver().update(LazyCouponContract.CouponsViewsDetails.CONTENT_URI, values, whereClause, selectionArgs);
                    m_cursor.moveToNext();
                }
            }
        }
    }

    private void markClicksSynced(String clicksWhereClause, String[] clicksSelectionArgs) {
        ContentValues updateUpSyncValues = new ContentValues();
        updateUpSyncValues.put(LazyCouponContract.CouponsStatistics.COLUMN_COUPON_UP_SYNC, 1);
        getContext().getContentResolver().update(LazyCouponContract.CouponsClicksDetails.CONTENT_URI, updateUpSyncValues, clicksWhereClause, clicksSelectionArgs);
    }


    private void markViewsSynced(String viewsWhereClause, String[] viewsSelectionArgs) {
        ContentValues updateUpSyncValues = new ContentValues();
        updateUpSyncValues.put(LazyCouponContract.CouponsStatistics.COLUMN_COUPON_UP_SYNC, 1);
        getContext().getContentResolver().update(LazyCouponContract.CouponsViewsDetails.CONTENT_URI, updateUpSyncValues, viewsWhereClause, viewsSelectionArgs);
    }


    public static void configurePeriodicSync(Context context, int syncInterval, int flexTime) {
        Account account = getSyncAccount(context);
        String authority = context.getString(R.string.couponviews);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            SyncRequest request = new SyncRequest.Builder().
                    syncPeriodic(syncInterval, flexTime).
                    setSyncAdapter(account, authority).
                    setExtras(new Bundle()).build();
            ContentResolver.requestSync(request);
        } else {
            ContentResolver.addPeriodicSync(account,
                    authority, new Bundle(), syncInterval);
        }
    }


    public static void syncImmediately(Context context) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        ContentResolver.requestSync(getSyncAccount(context),
                context.getString(R.string.couponviews), bundle);

        Log.d("views", "syncing");
    }


    public static Account getSyncAccount(Context context) {
        AccountManager accountManager =
                (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);

        Account newAccount = new Account(
                context.getString(R.string.app_name), context.getString(R.string.sync_account_type));

        if (null == accountManager.getPassword(newAccount)) {

        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
            if (!accountManager.addAccountExplicitly(newAccount, "", null)) {
                return null;
            }
            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call ContentResolver.setIsSyncable(account, AUTHORITY, 1)
             * here.
             */
            Log.d("account", "created");
            onAccountCreated(newAccount, context);
        } else {
            Log.d("account", "not created");
        }

        return newAccount;
    }

    private static void onAccountCreated(Account newAccount, Context context) {


        /*
         * Since we've created an account
         */
        CouponViewsSycnAdapter.configurePeriodicSync(context, SYNC_INTERVAL, SYNC_FLEXTIME);

        /*
         * Without calling setSyncAutomatically, our periodic sync will not be enabled.
         */
        ContentResolver.setSyncAutomatically(newAccount, LazyCouponContract.CONTENT_AUTHORITY, true);

        /*
         * Finally, let's do a sync to get things started
         */
        //  syncImmediately(context);
    }

    public static void initializeSyncAdapter(Context context) {

        getSyncAccount(context);
    }

}



