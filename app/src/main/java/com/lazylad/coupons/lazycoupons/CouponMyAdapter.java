package com.lazylad.coupons.lazycoupons;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Amud on 25/01/16.
 */


public class CouponMyAdapter extends RecyclerView.Adapter<CouponMyAdapter.ViewHolder> {

    private ArrayList<CouponMySnippet> m_CouponMyList;

    private Activity m_activity;
    static OnItemClickListener mItemClickListener;


    public CouponMyAdapter(Context context, ArrayList<CouponMySnippet> couponMyList) {
        m_activity = (Activity) context;
        this.m_CouponMyList = couponMyList;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.coupon_my_list_layout, parent, false);

        Log.i("view", "created");

        return new ViewHolder(v, viewType);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if (!m_CouponMyList.isEmpty()) {
            String coupon_code=m_CouponMyList.get(position).coupon_code;
            final String couponName = m_CouponMyList.get(position).coupon.name;
            String couponShortDesc= m_CouponMyList.get(position).coupon.short_desc;


            holder.m_couponNameTextView.setText(couponName);
            holder.m_couponShortDescTextView.setText(couponShortDesc);
            holder.m_couponCode.setText("Coupon Code : "+coupon_code);


            String image_url =  m_CouponMyList.get(position).coupon.img_address;
            boolean image_flag =  m_CouponMyList.get(position).coupon.img_flag;
            WindowManager wm = (WindowManager) m_activity.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;


            if (image_flag && image_url != null && !image_url.isEmpty()) {
                Picasso.with(m_activity)
                        .load(image_url)
                        .resize(width, 0)
                        .placeholder(R.drawable.loading_big)
                        .error(R.drawable.loading_big)
                        .into(holder.m_couponImage);
            }

        }
    }


    @Override
    public int getItemCount() {
        return m_CouponMyList.size();
    }

    public void onItemClick(View view, int position) {
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView m_couponNameTextView;
        TextView m_couponShortDescTextView;
        TextView m_couponCode;
        ImageView m_couponImage;

        public ViewHolder(View itemView, int flag) {
            super(itemView);
            m_couponNameTextView = (TextView) itemView.findViewById(R.id.coupon_name);
            m_couponShortDescTextView = (TextView) itemView.findViewById(R.id.coupon_short_desc);
            m_couponCode = (TextView) itemView.findViewById(R.id.coupon_code);
            m_couponImage = (ImageView) itemView.findViewById(R.id.coupon_image);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getPosition());
            }
        }
    }

    public Object getItem(int position) {
        if (m_CouponMyList != null) {
            return m_CouponMyList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (m_CouponMyList != null) {
            return position;
        }
        return 0;
    }
}




